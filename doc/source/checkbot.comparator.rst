checkbot.comparator package
===========================

Submodules
----------

checkbot.comparator.core module
-------------------------------

.. automodule:: checkbot.comparator.core
    :members:
    :undoc-members:
    :show-inheritance:

checkbot.comparator.utils module
--------------------------------

.. automodule:: checkbot.comparator.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: checkbot.comparator
    :members:
    :undoc-members:
    :show-inheritance:
