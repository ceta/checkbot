checkbot package
================

Subpackages
-----------

.. toctree::

    checkbot.common
    checkbot.comparator
    checkbot.dao
    checkbot.extractor
    checkbot.orchestrator
    checkbot.renderer
    checkbot.server
    checkbot.standalone

Module contents
---------------

.. automodule:: checkbot
    :members:
    :undoc-members:
    :show-inheritance:
