checkbot.standalone package
===========================

Module contents
---------------

.. automodule:: checkbot.standalone
    :members:
    :undoc-members:
    :show-inheritance:
