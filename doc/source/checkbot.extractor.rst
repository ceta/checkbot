checkbot.extractor package
==========================

Submodules
----------

checkbot.extractor.core module
------------------------------

.. automodule:: checkbot.extractor.core
    :members:
    :undoc-members:
    :show-inheritance:

checkbot.extractor.utils module
-------------------------------

.. automodule:: checkbot.extractor.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: checkbot.extractor
    :members:
    :undoc-members:
    :show-inheritance:
