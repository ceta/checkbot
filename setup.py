from setuptools import setup

setup(
    name='checkbot',
    version="0.1.0",
    description='Contract checkbot. Uses ta doc/docx document and extracts the content in it,' +
    'additionally performs a comparison between 2 documents, using the extracted content obtained in the first step, and ' +
    'outputs a dict/json object containing the deltas, and error descriptions.',

    author='@tatibloom',
    author_email='a.lopezgonzalez@reply.it',
    license='MIT',
    classifiers=[
        'Development Status :: 1 - Alpha',
        'Natural Language :: English',
        'Intended Audience :: Commercial',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6.5',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Python Modules',
        ],
    install_requires=['lxml', 'flask', 'requests', 'pytest'],
    packages=['checkbot', 'checkbot.common', 'checkbot.comparator', 'checkbot.dao', 'checkbot.extractor', 'checkbot.orchestrator', 'checkbot.renderer', 'checkbot.server', 'checkbot.standalone'],
    setup_requires=[]
)
