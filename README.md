# Contract checkbot

----
## Setup

To setup the application, a valid environment with python and pip should be ready, with these packages in place, we now create a virtual environment for our project.

First, we need to clone the repository:

    git clone https://gitlab.com/x/checkbot.git

To isolate our app (and as a good practice) from the other python files on the system we'll set up a virtual environment:

    cd checkbot
    python -m venv checkbotenv

To activate it:

    source checkbotenv/bin/activate

We've downloaded a working copy and the virtual environment has been created, now we need to install the app's required modules:

    pip install -r requirements.txt

If we want to install our application as a python module:

    python setup.py build_ext --inplace
    python setup.py install

At this point the application is ready and can be used in development and testing mode, however, in a production environment we need to use a WSGI container, for instance **gunicorn**:

    pip install gunicorn

---
## Start

The app can be used in 2 different modes, **server** and **standalone**.

----
### Standalone version

The standalone version uses the standalone version of the *converter service*, thus, it needs a jar named **checkbot.jar** and a database named **checkbot.db** (can be changed with the execution options).

The application will scan the current folder for all the files needed (checkbot.jar, checkbot.db and *.doc/*.docx).

Run the following command to see all options provided:

    python -m checkbot.standalone --help

To run the application:

    python -m checkbot.standalone -L "locale" -B "base contract" -U "updated contract" -D "database.db"

To generate the standalone as an executable (.exe):

    pip install pyinstaller
    cd checkbot/standalone
    pyinstaller --onefile __main__.py

A \__main__.exe file is created in checkbot/standalone/dist, rename it to checkbot.exe

To run the executable:

    checkbot -L "locale" -B "base contract" -U "updated contract" -D "database.db"


----
### Server version

The server version exposes 5 URLs in total, the first 2 are always available, the remaining 3, only if the parameter expose_all is set to True:

* GET aliveness/ping
* POST checkbot/<locale>
* POST checkbot/extract/<locale>
* POST checkbot/compare/<locale>
* POST checkbot/render

The application uses a configuration file named **config.cfg**, (*a python script with a different extension, imports or any other
python syntax can be used inside*). The configuration file location is set in an environment variable named **CHECKBOT_CONFIG**.

To set the environment variable:

    On Unix: export CHECKBOT_CONFIG=/path/to/config.cfg
    On windows: setx CHECKBOT_CONFIG /path/to/config.cfg

The configuration file contains information such the *mode* in which the application is going to be started, all URLs needed
to run the entire process, the supported file extensions and the database location. In a production environment the parameters
THREADED, DEBUG and TEST should be set to False.

Finally, to start the application with the default WSGI container (Werkzeug):

    On Unix: export FLASK_APP=checkbot.server.application:app
    On Windows: set FLASK_APP=checkbot.server.application:app
    flask run

Using Gunicorn (use one of the following):

    gunicorn checkbot.server.application:app
    gunicorn -b 127.0.0.1:4000 checkbot.server.application:app -p rocket.pid -D