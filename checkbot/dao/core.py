import sqlite3
import checkbot
import os.path
import sys

from checkbot.common.utils import Constant


def get_connection():
    """
    Gets a database connection to the SQLite database specified in the parameter DB_FILENAME in the application
    configuration file.

    :return: Connection object or None
    """
    if not os.path.exists(checkbot.app.config['DB_FILENAME']):
        sys.stdout.write('An error occurred when accessing the Database <{}>\n'.format(checkbot.app.config['DB_FILENAME']))
        raise ValueError('An error occurred when accessing the Database <{}>'.format(checkbot.app.config['DB_FILENAME']))

    conn = sqlite3.connect(checkbot.app.config['DB_FILENAME'])
    return conn


def select_conf(locale, category):
    """
    Queries table conf by locale and category.

    :param str locale: Language/Country to properly select params, enums, tables and more.
    :param str category: Category to filter out the configurations: params, enums, tables and columns to be ignored.
    :return: List of tuples (id, code, value)
    """
    conn = get_connection()

    try:
        cur = conn.cursor()
        cur.execute('SELECT id, code, value FROM conf WHERE locale = ? AND category = ?', (locale, category, ))
        rows = cur.fetchall()
        conn.close()

        return rows
    except Exception as dae:
        sys.stderr.write('{}\n'.format(dae))
        raise ValueError('An error occurred when querying the database: table <conf>.')


def select_message(locale, category, code=None):
    """
    Queries table message by locale, category and code.

    :param str locale: Language/Country to properly select messages.
    :param str category: Category to filter out the messages: headers or error messages.
    :param str code: Code identifying a unique message.
    :return: List of tuples (code, value)
    """
    conn = get_connection()

    try:
        cur = conn.cursor()

        sql = 'SELECT code, value FROM message WHERE locale = ? AND category = ?'
        params = (locale, category, )

        if code:
            sql += ' AND code = ?'
            params += (code, )

        cur.execute(sql, params)
        rows = cur.fetchall()
        conn.close()

        return rows
    except Exception as dae:
        sys.stderr.write('{}\n'.format(dae))
        raise ValueError('An error occurred when querying the database: table <message>.')


def select_header(locale, code=None):
    """
    Returns a list with the headers specified by the locale and code.

    :param str locale: Language/Country to properly select headers.
    :param code: The code (or id) of the group.
    :return: Result as list
    """
    rows = select_message(locale, Constant.HEADER, code)

    return [row[1] for row in rows] if rows else []


def select_escaped_characters():
    """
    Returns a list with the headers specified by the locale and code.

    :return: Result as list
    """
    rows = select_message('', Constant.ESCAPED_CHARACTERS)

    if len(rows) > 1:
        sys.stdout.write('Escaped characters: More than one configuration found. Discarding everything but the first one.\n')

    if rows:
        return rows[0][1].encode().decode('unicode_escape')

    return None


def select_error_messages(locale):
    """
    Returns a list with the error messages specified by the locale.

    :param str locale: Language/Country to properly select error messages.
    :return: Result as list
    """
    rows = select_message(locale, Constant.ERROR_MESSAGE)

    try:
        data = {}
        for row in rows:
            data.update({row[0]: row[1]})

        return data
    except Exception as e:
        sys.stderr.write('{}\n'.format(e))
        raise ValueError('Error formatting error messages.')


def select_generic(locale, category, multiple=False):
    """
    Format result from query conf by category and locale.

    :param str locale: Language/Country to properly select params, enums, tables and more.
    :param str category: Category to filter out the results: params, enums, tables and columns to be ignored.
    :param multiple: If the values returned are single or multiple (list).
    :return: Result as dictionary.
    """
    rows = select_conf(locale, category)

    try:
        data = {}
        for row in rows:
            root = data.get(row[0]) if data.get(row[0]) else {}

            if root:
                if root.get(row[1]):
                    values = root.get(row[1]) + [row[2]] if multiple else row[2]
                else:
                    values = [row[2]] if multiple else row[2]

                root.update({row[1]: values})
            else:
                values = [row[2]] if multiple else row[2]
                data.update({row[0]: {row[1]: values}})

        return data
    except Exception as e:
        sys.stderr.write('{}\n'.format(e))
        raise ValueError('Error formatting {}.'.format(category.lower()))


def select_ignored_cols(locale):
    """
    Format result from query the ignored columns configured in table conf.

    :param str locale: Language/Country to properly select the ignored columns.
    :return: A dictionary with all the ignored columns.
    """
    rows = select_conf(locale, Constant.IGNORED_COLS)

    try:
        data = {}
        for row in rows:
            values = data.get(row[0]) + [row[2]] if data.get(row[0]) else [row[2]]
            data.update({row[0]: values})

        return data
    except Exception as e:
        sys.stderr.write('{}\n'.format(e))
        raise ValueError('Error formatting ignored columns.')


def select_enums(locale):
    """
    Format result from query the enums configured in table conf.

    :param str locale: Language/Country to properly select the enums.
    :return: A dictionary with all the enums.
    """
    return select_generic(locale, Constant.ENUMS, multiple=True)


def select_params(locale):
    """
    Format result from query the params configured in table conf.

    :param str locale: Language/Country to properly select the params.
    :return: A dictionary with all the params.
    """
    return select_generic(locale, Constant.PARAMS)


def select_tables(locale):
    """
    Format result from query the tables configured in table conf.

    :param str locale: Language/Country to properly select the tables.
    :return: A dictionary with all the tables.
    """
    return select_generic(locale, Constant.TABLES)
