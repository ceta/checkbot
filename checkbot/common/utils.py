import copy
import html
import re
import sys

from lxml import etree


class Constant:
    """
    Class to hold all constants used in the application.
    """
    ARTICLE_KEY = 'A'
    PARAGRAPH_KEY = 'P'
    SUB_PARAGRAPH_KEY = 'S'
    SUB_SUB_PARAGRAPH_KEY = 'X'

    ARTICLE_MASK = '<article>'
    PARAGRAPH_MASK = '<paragraph>'
    SUB_PARAGRAPH_MASK = '<sub_paragraph>'
    SUB_SUB_PARAGRAPH_MASK = '<sub_sub_paragraph>'
    TEXT_MASK = '<text>'
    INT_MASK = '<int>'
    FLOAT_MASK = '<float>'
    NUMERIC_MASK = '<numeric>'
    DATE_MASK = '<date>'
    ENUM_MASK = '<enum:(.*)>'

    IGNORED_COLS = 'IGNORED_COLS'
    ENUMS = 'ENUMS'
    PARAMS = 'PARAMS'
    TABLES = 'TABLES'

    HEADER = 'HEADER'
    ERROR_MESSAGE = 'ERROR_MESSAGE'
    ESCAPED_CHARACTERS = 'ESCAPED_CHARACTERS'
    CONTRACT_ITEM_NON_AVAILABLE = 'NA'
    ERROR_MISSING_VALUE = 'M'
    ERROR_WRONG_VALUE = 'C'

    SEPARATOR_MASK = '||||'


class Pattern:
    """
    Class to hold all patterns used in the application.
    """
    # ARTICLE_0 = '^[1-9][0-9]*'  # 2
    ARTICLE_1 = '^{}.*?([1-9]{{1}}[0-9]{{0,1}})\s*([a-zA-Z]+)'  # X: Article 2 | Articolo 2 | Artikel 2
    ARTICLE_2 = '^([1-9]{1}[0-9]{0,1})\s*\.*\s*([A-Z]+)'  # 2 Abc
    ARTICLE = '({}|{})'.format(ARTICLE_1, ARTICLE_2)
    PARAGRAPH = '^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)'  # 2.5
    SUB_PARAGRAPH = '^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)'  # 2.5.2
    SUB_SUB_PARAGRAPH = '^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)'  # 2.5.2.7

    TEXT = '(.*)'  # anything
    INT = '([0-9]+)'  # 0 1 2 3 4 5 6 7 8 9
    FLOAT = '([0-9]+[.,][0-9]+)'  # 3.35 3,55
    NUMERIC = '({}|{})'.format(FLOAT, INT)  # 3.35 3,55 or 58 15 23
    DATE = '((0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.]((19|20)\d\d|\d\d))'  # dd mm yyyy dd-mm-yy
    ENUM = '((?:{}))'


def get_pattern(param, enums=None):
    """
    Generates a string representing a regex-like pattern.

    :param str param: To generate the pattern according the params's data-type.
    :param enums: To generate the pattern including the values for the configured enums.
    :return: A formatted string
    """
    # Escaping special characters []{}()\ *+?.,|^$#
    param = param.lower()
    pattern = re.escape(param)
    # Formatting generic pattern
    pattern = '.*?({}).*?'.format(pattern)  # ? to avoid greedy search

    # Replace articles
    pattern = pattern.replace(re.escape(Constant.ARTICLE_MASK), Pattern.ARTICLE)
    # Replace paragraphs
    pattern = pattern.replace(re.escape(Constant.PARAGRAPH_MASK), Pattern.PARAGRAPH)
    # Replace sub_paragraphs
    pattern = pattern.replace(re.escape(Constant.SUB_PARAGRAPH_MASK), Pattern.SUB_PARAGRAPH)
    # Replace sub_sub_paragraphs
    pattern = pattern.replace(re.escape(Constant.SUB_SUB_PARAGRAPH_MASK), Pattern.SUB_SUB_PARAGRAPH)

    # Replace text
    pattern = pattern.replace(re.escape(Constant.TEXT_MASK), Pattern.TEXT)
    # Replace floats
    pattern = pattern.replace(re.escape(Constant.FLOAT_MASK), Pattern.FLOAT)
    # Replace ints
    pattern = pattern.replace(re.escape(Constant.INT_MASK), Pattern.INT)
    # Replace numeric
    pattern = pattern.replace(re.escape(Constant.NUMERIC_MASK), Pattern.NUMERIC)
    # Replace dates
    pattern = pattern.replace(re.escape(Constant.DATE_MASK), Pattern.DATE)
    # Replace enums
    key = re.compile(Constant.ENUM_MASK).search(param)
    if key and enums:
        enum_list = [x.lower() for x in get_value(enums, key.group(1))]
        pattern = pattern.replace(re.escape(key.group(0)),
                                  Pattern.ENUM.format('|'.join(enum_list)))

    return pattern


def get_value(dictionary, key):
    """
    Returns a value from <dictionary>, performs a search by <key> contemplating both, a regex and a plain search. The
    dictionary's keys are used as pattern, thus for the following dictionary, the key 'Something 3.5 and 827' will do
            {
                'Something <float> and <int>'  : {}
            }

    :param dict dictionary: Dictionary with data.
    :param str key: Dictionary's key.
    :return: Value from <dictionary>.
    """
    param = dictionary.get(key)
    key = key.lower()

    if param:
        return param
    else:
        param = [v for k, v in dictionary.items() if re.compile(get_pattern(k)).search(key)]

        if param:
            if len(param) > 1:
                sys.stdout.write('The pattern specified is too generic (more than one key encountered).\n')
                return dict()
            else:
                if isinstance(param[0], list):
                    param[0] = [re.sub(' ', '', x) for x in param[0]]
                elif isinstance(param[0], dict):
                    for k, v in param[0].items():
                        if isinstance(v, list):
                            v = [re.sub(' ', '', x) for x in v]
                        else:
                            v = re.sub(' ', '', v)
                        param[0].update({k: v})
                return param[0]
        else:
            return dict()


def get_html(element):
    """
    Returns the html string of the lxml.etree._Element. A copy is done to clone the element and to not modify the
    original one.

    :param lxml.etree._Element element: XML/HTML element
    :return: A string representation of the HTML element.
    """
    base_html = etree.tostring(copy.copy(element), encoding='unicode', method='html')  \
        .replace('\n', '')\
        .replace('\r', '')\
        .replace('\t', '')
    # .replace('&#13;', ' ')\
    # .replace('&#10;', ' ')\

    return html.unescape(base_html)


def get_text(element):
    """
    Returns the text inside an element, considering also the text between tags.
    Uses the method tostring (method='text') from lxml.etree that extracts the text from the first child (if any),
    and recursively adds the text in the tail. A copy is done to clone the element and to not modify the
    original one. First the html is extracted (and formatted) so as to fix the break lines
    generated during conversion.

    Added: ndiff does not escape characters, thus we need to fold multiple blank spaces, to let ndiff run without
    considering extra differences.

    :param lxml.etree._Element element: (XML) HTML element.
    :return: HTML's element's original text and "escaped" text.
    """
    base_text = etree.tostring(copy.copy(element), encoding='unicode', method='text')\
        .replace('\t', '')\
        .replace('\n', '')\
        .replace('\r', '')

    escaped_text = re.sub('(\s+|\u200b+)', ' ', base_text)

    return base_text, escaped_text


def get_styles(element, style):
    """
    Returns all styles used in the current element, as a list of strings.
        ['.p1{}', '.s1{}']

    :param lxml.etree._Element element: (XML) HTML element.
    :param str style: Document's style i.e <style></style>.
    :return: List of styles.
    """
    # Check style to spot Articles, only for simple pattern e.g 1 Savings
    classes = [element.attrib.get('class')]

    for inner_element in element.findall('.//'):
        classes.append(inner_element.attrib.get('class'))

    styles = list()
    for cls in classes:
        if not cls:
            continue
        p = re.compile('(.{}{{(.*?)}})'.format(cls)).search(style)
        if p:
            styles.append(re.sub('font-size:.*?\;', '', re.sub('font-family:.*?\;', '', p.group(1))))

    return styles


def format_filename(path, filename=''):
    """
    It formats the filename path by adding the final separator '/' if needed, and replacing '\' for '/'.

    :param str path: URI-based path
    :param str filename: filename
    :return: formatted path + filename
    """
    path = path.strip().replace('\\', '/')
    return path + filename if len(path) - 1 == path.rfind('/') else '{}/{}'.format(path, filename)


def get_file_name(filename):
    """
    Gets the file name (without the extension) from the file name given as a parameter.

    :param str filename: File name to be evaluated.
    :return: Filename.
    """
    return filename[0:filename.rfind('.')] if '.' in filename else None


def get_file_extension(filename):
    """
    Gets the file extension from the file name given as a parameter.

    :param str filename: File name to be evaluated.
    :return: File extension.
    """
    return filename[filename.rfind('.')+1:].lower() if '.' in filename else None
