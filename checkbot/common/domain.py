import re

from checkbot.common.utils import get_pattern, Constant


class Element:
    """
    class Element: Holds every (first-level) tag inside the HTML document.

    attributes:
        __id__  (str) : Used for (Contract) Articles, Paragraphs and Sub-Paragraphs (numeric id).
        mask    (str) : Used for (Contract) Articles, Paragraphs and Sub-Paragraphs (text id).
        text    (str) : Tag's text, considering also the text in nested tags
                 e.g From "<h1><span>Article 1 </span><span>Parties</span></h1>", would be "Article 1 Parties".
        text_h  (str) : Escaped text (replaces *special* characters with one blank space)
        html    (str) : Tag's html string
                 e.g. "<h1><span lang="EN-US" style="mso-ansi-language:EN-US">Article 1 Parties</span></h1>".
        styles  (str) : Element's style list.

    methods:
        __init__ : Constructor.
        __bool__ : True, if the object is not empty, False otherwise (previously: __nonzero__).
    """
    __id__ = mask = text = text_h = html = styles = None

    def __init__(self, *args):
        if args:
            self.__id__, self.mask, self.text, self.text_h, self.html, self.styles = args
        else:
            self.__id__ = self.mask = self.text = self.text_h = self.html = self.styles = None

    def __bool__(self):
        if not self.text and not self.html:
            return False
        else:
            return True


class Error:
    """
    class Error: Holds the error produced during comparison.

    attributes:
        contract_item  (str) : Category given in the list of parameters to evaluate.
        error_type     (str) : Type of error,
                              "Missing" if the text line or param was not present.
                              "Changed" if the text line or param was changed.
        x  (list(tuple(int))) : List of delta's positions of the base document. A position is given
                                as a pair (or tuple) meaning the <from>,<to> indices in which the base
                                text is different wrt updated one.
        y  (list(tuple(int))) : List of delta's positions of the updated document. A position is given
                                as a pair (or tuple) meaning the <from>,<to> indices in which the updated
                                text is different wrt base one.

    methods:
        __init__ : Constructor.
    """
    contract_item = error_type = x = y = None

    def __init__(self, *args):
        if args:
            self.contract_item, self.error_type, self.x, self.y = args
        else:
            self.contract_item = self.error_type = None
            self.x = self.y = list()


class Item:
    """
    class Item: Holds a sort of nested object i.e articles, paragraphs and sub-paragraphs (numerated
    or not) can include different elements (or content) so as to group all related text paragraphs and/or
    tables (not Contract paragraphs) in a single item, and give them a sort of context.

    attributes:
        article            (Element) : Contract's article.
        paragraph          (Element) : Contract's paragraph,
        sub_paragraph      (Element) : Contract's sub_paragraph.
        sub_sub_paragraph  (Element) : Contract's sub_paragraph no-numerated element.
        content      (list(Element)) : All article|paragraph|sub_paragraph|sub_sub_paragraph text paragraphs.
        styles  (               str) : Item's style list.

    methods:
        __init__          : Constructor.
        add        (void) : Adds a new element to the current item.
        to_array   (list) : Returns the item's content as a list of strings. If the item's contain a table, each row is
                            added to the list.
        search  (Element) : Returns an element given a text line, i.e if the Item's content include the text searched,
                            it will return the corresponding element.

    static methods:
        format_key           (str) : Returns a key given an article, paragraph, sub_paragraph and/or sub_sub_paragraph
        format_table         (str) : Returns a string representation of an Element of type table (or list)
        filter_table   (list(str)) : Returns a filtered table i.e. from the base table given as a parameter, it filters
                                     or ignores the columns that have been previously configured.

    """
    article = paragraph = sub_paragraph = sub_sub_paragraph = content = html = styles = None

    def __init__(self, *args):
        self.article, self.paragraph, self.sub_paragraph, self.sub_sub_paragraph = args
        self.content = list()
        self.html = ''
        self.styles = list()

    def add(self, text, text_h, html, styles):
        self.content.append(Element(None, None, text, text_h, html, styles))
        self.html += html
        self.styles += styles

    def to_array(self, escaped=True, ignored_cols=None):
        content = list()

        for c in self.content:
            if isinstance(c.text, list):
                if ignored_cols:
                    content += Item.filter_table(c.text, ignored_cols)
                else:
                    content += ['\t'.join(row) for row in c.text]
            else:
                content.append(c.text_h.rstrip() if escaped else c.text.rstrip())

        return content

    def search(self, text, ignored_cols=None):
        for i, c in enumerate(self.content):
            if isinstance(c.text, list):
                if ignored_cols:
                    if text in Item.filter_table(c.text, ignored_cols):
                        return c, i
                else:
                    if text in ['\t'.join(row) for row in c.text]:
                        return c, i
            elif c.text.strip() == text.strip():
                    return c, i

        return Element(), -1

    @staticmethod
    def format_key(article, paragraph, sub_paragraph, sub_sub_paragraph):
        return Constant.SEPARATOR_MASK.join(
            filter(None, (article.mask, paragraph.mask, sub_paragraph.mask, sub_sub_paragraph.mask)))

    @staticmethod
    def filter_table(table, ignored_cols):
        content = ['\t'.join(table[0])]

        for row in table[1:]:
            formatted_row = []
            for j, col in enumerate(row):
                header = table[0][j].strip().lower()
                ignored_col = [header for e in ignored_cols if re.compile(get_pattern(e)).search(header)]
                if not ignored_col:
                    formatted_row.append(col)
            content.append('\t'.join(formatted_row))

        return content
