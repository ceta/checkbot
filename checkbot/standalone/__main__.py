import checkbot
import json
import optparse
import os
import subprocess
import sys
import webbrowser

from checkbot.common.utils import get_file_extension, get_file_name, format_filename
from checkbot.server.utils import AppJSONEncoder


"""
The standalone version uses the standalone version of the converter service, thus it needs a jar
named checkbot.jar and a database named checkbot.db (can be changed with the execution options).
The application will scan the current folder for all the files needed.

To use it as a python package, first it needs to be installed:
    - python setup.py build_ext --inplace
    - python setup.py install
    
    To run the application:
    - python -m checkbot.standalone -L "locale" -B "base contract" -U "updated contract" -D "database.db"
    - Use python -m checkbot.standalone --help for help.

To generate the standalone as an executable (.exe) version use:
    - Install the python package
    - cd checkbot/standalone
    - pyinstaller --onefile __main__.py
    - A __main__.exe file is created in checkbot/standalone/dist, rename it to checkbot.exe
    
    To run the application:
    - checkbot -L "locale" -B "base contract" -U "updated contract" -D "database.db"
    - Use checkbot --help for help.
"""


class App:
    config = None


default_base_file = 'TERMICA MILAZZO GY18 Utente.doc'
default_updated_file = 'TERMICA MILAZZO GY18 WCG.DOC'
default_db = 'checkbot.db'
default_locale = 'IT_it'
default_render = 'true'

parser = optparse.OptionParser()
parser.add_option("-D", "--db",
                  help="Database name." +
                       "[default %s]" % default_db,
                  default=default_db)
parser.add_option("-B", "--base_contract",
                  help="Filename of base contract" +
                       "[default %s]" % default_base_file,
                  default=default_base_file)
parser.add_option("-U", "--updated_contract",
                  help="Filename of updated contract " +
                       "[default %s]" % default_updated_file,
                  default=default_updated_file)
parser.add_option("-L", "--locale",
                  help="Locale " +
                       "[default %s]" % default_locale,
                  default=default_locale)
parser.add_option("--render",
                  help="If the process needs to render an HTML page, or just generate the json file. " +
                       "[default %s]" % default_render,
                  default=default_render)

options, _ = parser.parse_args()

try:
    sys.stdout.write('Starting process ...\n')
    path = os.path.dirname(os.path.realpath(__file__))
    # path = 'C:\\Users\\a.lopezgonzalez\\Documents\\Workspaces\\python-workspaces\\checkbot\\data'
    sys.stdout.write('Current path - {}\n'.format(path))

    checkbot.app = App()
    checkbot.app.config = {'SUPPORTED_TYPES': ['doc', 'docx'],
                           'DB_FILENAME': format_filename(path, options.db),
                           'MINIMUM_PARAGRAPH_SIZE': 125,
                           'EXPOSE_ALL': False,
                           'SERVER_APP': False,
                           'RENDER': options.render.lower() == 'true' or options.render.lower() == 't' or options.render.lower() == 'yes' or options.render.lower() == 'y'}

    from checkbot.extractor.core import extract
    from checkbot.comparator.core import compare
    from checkbot.renderer.core import render
    from checkbot.orchestrator.core import build

    sys.stdout.write('Contract (base): {}\n'.format(options.base_contract))
    sys.stdout.write('Contract (updated): {}\n'.format(options.updated_contract))

    if get_file_extension(options.base_contract) not in checkbot.app.config['SUPPORTED_TYPES'] and \
            get_file_extension(options.updated_contract) not in checkbot.app.config['SUPPORTED_TYPES']:
        raise IOError('Not supported file extension.')

    subprocess.call(['java', '-jar', format_filename(path, 'checkbot.jar'), format_filename(path),
                    get_file_name(options.base_contract), get_file_extension(options.base_contract)])
    base_contract = open(format_filename(path, get_file_name(options.base_contract) + '.html'), encoding='utf-8').read()
    os.remove(format_filename(path, get_file_name(options.base_contract) + '.html'))

    subprocess.call(['java', '-jar', format_filename(path, 'checkbot.jar'), format_filename(path),
                    get_file_name(options.updated_contract), get_file_extension(options.updated_contract)])
    updated_contract = open(format_filename(path, get_file_name(options.updated_contract) + '.html'), encoding='utf-8').read()
    os.remove(format_filename(path, get_file_name(options.updated_contract) + '.html'))

    extracted_data = extract(options.locale, base_contract, updated_contract)

    compared_data = compare(options.locale, extracted_data)

    formatted_data = build(compared_data)

    if checkbot.app.config['RENDER']:
        ext = '.html'
        result = render(formatted_data)
    else:
        ext = '.json'
        result = json.dumps(formatted_data, cls=AppJSONEncoder)

    with open(format_filename(path, 'output' + ext), 'w', encoding='utf-8') as f:
        f.write(result)
    f.close()

    sys.stdout.write('Output in <{}>\n'.format('file://{}'.format(format_filename(path, 'output' + ext))))
    webbrowser.open_new_tab('file://{}'.format(format_filename(path, 'output' + ext)))

    sys.stdout.write('Ending process ...\n')
except ValueError as e:
    sys.stderr.write('{}\n'.format(e))
    raise
