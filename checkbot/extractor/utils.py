import re
import sys

from checkbot.common.utils import Constant, Pattern, get_text, get_styles


def get_info(element, header, style) -> tuple:
    """
    Performs a pattern validation to obtain the element's type, numeric ID and mask (text ID). The returned tuple
    has the following format:
        [0] Element's type (A, P, S or X)
        [1] Element's numeric ID
        [3] Element's mask ID

    :param checkbot.common.domain.Element element element:
    :param list header: A list with all possible headers, filtered by locale.
    :param str style: Document's style.
    :return: Tuple.
    """
    text, _ = get_text(element)
    text = text.strip()

    sub_sub_paragraph = re.compile(Pattern.SUB_SUB_PARAGRAPH).search(text)
    if sub_sub_paragraph:
        return Constant.SUB_SUB_PARAGRAPH_KEY, sub_sub_paragraph.group(1), text.replace(sub_sub_paragraph.group(1), '').replace(' ', '').lower()

    sub_paragraph = re.compile(Pattern.SUB_PARAGRAPH).search(text)
    if sub_paragraph:
        return Constant.SUB_PARAGRAPH_KEY, sub_paragraph.group(1), text.replace(sub_paragraph.group(1), '').replace(' ', '').lower()

    paragraph = re.compile(Pattern.PARAGRAPH).search(text)
    if paragraph:
        return Constant.PARAGRAPH_KEY, paragraph.group(1), text.replace(paragraph.group(1), '').replace(' ', '').lower()

    article = re.compile(Pattern.ARTICLE_1.format('|'.join(header))).search(text.lower())
    if article:
        return Constant.ARTICLE_KEY, article.group(1), text.replace(article.group(1), '').replace(' ', '').lower()

    article = re.compile(Pattern.ARTICLE_2).search(text)
    if article:
        # Check style to spot Articles, only for simple pattern e.g 1 Savings
        styles = get_styles(element, style)

        for s in styles:
            p = re.compile('\{(.*?)\}').search(s)
            if p and ('bold' in p.group(1) or 'underline' in p.group(1)):
                return Constant.ARTICLE_KEY, article.group(1), text.replace(article.group(1), '').replace(' ', '').lower()

        # return Constant.ARTICLE_KEY, article.group(1), re.sub(Pattern.ARTICLE_2, '#', text).replace(' ', '')

    return None, None, None


def check_current_id(cur_id, cur_type, prevs):
    """
    Validates if the given id <cur_id> is correct, to do so performs a simple verification of the document numeration i.e if the
    previous id of an article is 3, the attempted given id should be 4, the same for paragraphs, and sub-paragraphs.
    :param str cur_id: Attempted ID to be evaluated.
    :param str cur_type: Current element's type: A (for article), P (for paragraph) and S (for sub-paragraph).
    :param dict prevs: Dict with each type's last ID.
    :return: True, if the id being evaluated is valid, False, if not.
    """
    if not cur_id or not cur_type:
        return False

    # Fix: When the HTML comes with different enumeration

    if cur_type == Constant.ARTICLE_KEY:
        article_id = int(cur_id)

        prev = prevs.get(Constant.ARTICLE_KEY)
        prev_article_id = int(prev)

        if not (article_id == prev_article_id + 1):
            sys.stdout.write('The article is misnumbered. Got {}, should be {}.\n'.format(article_id, prev_article_id + 1))

        return True

    if cur_type == Constant.PARAGRAPH_KEY:
        article_id = int(cur_id[0:cur_id.find('.')])
        paragraph_id = int(cur_id[cur_id.rfind('.')+1:])

        prev = prevs.get(Constant.PARAGRAPH_KEY)
        prev_article_id = int(prev[0:prev.find('.')])
        prev_paragraph_id = int(prev[prev.rfind('.')+1:])

        if not (article_id == prev_article_id and paragraph_id == prev_paragraph_id + 1):
            sys.stdout.write('The paragraph is misnumbered. Got {}, should be {}.\n'.format('.'.join([str(article_id), str(paragraph_id)]), '.'.join([str(prev_article_id), str(prev_paragraph_id + 1)])))

        return True

    if cur_type == Constant.SUB_PARAGRAPH_KEY:
        article_id = int(cur_id[0:cur_id.find('.')])
        paragraph_id = int(cur_id[cur_id.find('.')+1:cur_id.rfind('.')])
        sub_paragraph_id = int(cur_id[cur_id.rfind('.') + 1:])

        prev = prevs.get(Constant.SUB_PARAGRAPH_KEY)
        prev_article_id = int(prev[0:prev.find('.')])
        prev_paragraph_id = int(prev[prev.find('.')+1:prev.rfind('.')])
        prev_sub_paragraph_id = int(prev[prev.rfind('.') + 1:])

        if not (article_id == prev_article_id and paragraph_id == prev_paragraph_id and sub_paragraph_id == prev_sub_paragraph_id + 1):
            sys.stdout.write('The sub-paragraph is misnumbered. Got {}, should be {}.\n'.format('.'.join([str(article_id), str(paragraph_id), str(sub_paragraph_id)]), '.'.join([str(prev_article_id), str(prev_paragraph_id), str(prev_sub_paragraph_id + 1)])))

        return True

    return False
    # prev_id = prevs.get(cur_type)
    # cur_id = int(cur_id[cur_id.rfind('.')+1:]) if cur_type == Constant.PARAGRAPH_KEY or cur_type == Constant.SUB_PARAGRAPH_KEY else int(cur_id)

    # return True if cur_id == prev_id + 1 else False


def update_index(index, article_id, paragraph_id, sub_paragraph_id, sub_sub_paragraph_id):
    if article_id in index:
        article = index.get(article_id)

        if paragraph_id:
            if paragraph_id in article:
                paragraph = article.get(paragraph_id)

                if sub_paragraph_id:
                    if sub_paragraph_id in paragraph:
                        sub_paragraph = paragraph.get(sub_paragraph_id)

                        if sub_sub_paragraph_id:
                            if sub_sub_paragraph_id not in sub_paragraph:
                                sub_paragraph.append(sub_sub_paragraph_id)
                            paragraph.update({sub_paragraph_id: sub_paragraph})
                    else:
                        paragraph.update({sub_paragraph_id: []})

                    article.update({paragraph_id: paragraph})
            else:
                article.update({paragraph_id: {}})

            index.update({article_id: article})
    else:
        index.update({article_id: {}})

    return index


def index_tostring(index):
    result = list()
    for k_a, v_a in index.items():
        if v_a:
            for k_p, v_p in v_a.items():
                if v_p:
                    for k_s, v_s in v_p.items() :
                        if v_s:
                            for k_x in v_s:
                                result.append(Constant.SEPARATOR_MASK.join([k_a, k_p, k_s, k_x]))
                        else:
                            result.append(Constant.SEPARATOR_MASK.join([k_a, k_p, k_s]))
                else:
                    result.append(Constant.SEPARATOR_MASK.join([k_a, k_p]))
        else:
            result.append(k_a)

    return result
