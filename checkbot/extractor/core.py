import checkbot
import copy
import sys

from flask import request, jsonify
from io import StringIO
from lxml import etree

from checkbot.common.domain import Item, Element
from checkbot.common.utils import Constant, get_text, get_html, get_styles
from checkbot.dao.core import select_header
from checkbot.extractor.utils import get_info, check_current_id, update_index, index_tostring


def extract_table(element):
    """
    Returns a matrix-like representation of the HTML table. Additionally, it updates the colspan/rowspan of
    the HTML element (of the etree element), so as to reflect the real number of rows and columns and be able
    to perform the comparison of the elements' text without risks.

    :param lxml.etree.HtmlElement element: HTML Table element
    :return: A matrix-like representation of the HTML table.
    """

    rows = element.findall('tr')
    rows = rows if rows else element.findall('tbody/tr')
    table = dict()

    # To format the html table as well (colspan/rowspan)
    _table = copy.copy(element)
    _rows = _table.findall('tr')
    _rows = _rows if _rows else _table.findall('tbody/tr')
    _ = [tr.remove(td) for tr in _rows for td in tr.findall('td')]

    for r, row in enumerate(rows):
        cols = row.findall('td')

        for col in cols:
            text, _ = get_text(col)

            _col = copy.deepcopy(col)
            _col.attrib['rowspan'] = '1'
            _col.attrib['colspan'] = '1'

            row_span = int(col.get('rowspan')) if col.get('rowspan') else 1
            col_span = int(col.get('colspan')) if col.get('colspan') else 1

            for i in range(row_span):
                _row = _table.find('tr[{}]'.format(r + i + 1))
                _row = _row if _row is not None else _table.find('tbody/tr[{}]'.format(r + i + 1))
                for _ in range(col_span):
                    row_cols = table.get(str(r + i)) if table.get(str(r + i)) else []
                    row_cols.append(text)
                    table.update({str(r + i): row_cols})

                    index = len(_row.findall('td')) + 1
                    _row.insert(index, copy.deepcopy(_col))

    return [row for row in list(table.values())], get_html(_table)


def extract_content(tree, index, header, document_style):
    """
     Iterates over every <div> HTML element, so as to get the elements inside and start the extraction process.

    :param list tree                : A XML/HTML document.
    :param dict index               : Document's index (combined).
    :param list header              : Header for articles, paragraphs and sub-paragraphs.
    :param string document_style    : Document's style.
    :return                         : A dict representation of the document.
    """
    data = dict()

    prevs = {Constant.ARTICLE_KEY: '0', Constant.PARAGRAPH_KEY: '0.0', Constant.SUB_PARAGRAPH_KEY: '0.0.0'}  # just to double check the structure
    article = Element('0', '', '', '', '', [])
    paragraph = sub_paragraph = sub_sub_paragraph = Element()

    for element in tree:
        if element.tag == 'div':
            for e in element:
                index, data, article, paragraph, sub_paragraph, sub_sub_paragraph = process(e, document_style, header, index, data, prevs, article, paragraph, sub_paragraph, sub_sub_paragraph)
        else:
            index, data, article, paragraph, sub_paragraph, sub_sub_paragraph = process(element, document_style, header, index, data, prevs, article, paragraph, sub_paragraph, sub_sub_paragraph)

    return index, data


def process(element, document_style, header, index, data, prevs, article, paragraph, sub_paragraph, sub_sub_paragraph):
    """
     Creates a dictionary out of ElementTree (XML/HTML like document), considering the following format:
        key  : Concatenated ID, considering that non-enumerated items can be repeated. e.g article|paragraph|sub-paragraph|sub-sub-paragraph
        value: Item's text from all "nested" elements

    :param checkbot.common.domain.Element element   : Element to be evaluated.
    :param string document_style                    : Document's style.
    :param list header                              : Header for articles, paragraphs and sub-paragraphs (filtered by locale).
    :param dict index                               : Document's index (combined).
    :param data                                     : A dict representation of the document being evaluated.
    :param prevs                                    : A dict containing the current article, paragraph and sub-paragraph ids.
    :param article                                  : The current article.
    :param paragraph                                : The current paragraph.
    :param sub_paragraph                            : The current sub_paragraph.
    :param sub_sub_paragraph                        : The current sub_sub_paragraph.
    :return                                         : A dict representation of the document.
    """
    cur_type, cur_id, cur_mask = get_info(element, header, document_style)

    text, text_h = get_text(element)
    styles = get_styles(element, document_style)
    html = get_html(element)

    # To avoid to process empty lines
    if not text:
        return index, data, article, paragraph, sub_paragraph, sub_sub_paragraph

    if not check_current_id(cur_id, cur_type, prevs):
        cur_id = cur_type = None

    # Chapter or Articles
    if cur_type == Constant.ARTICLE_KEY:
        article = Element(cur_id, cur_mask, text, text_h, html, styles)
        paragraph = sub_paragraph = sub_sub_paragraph = Element()
        prevs.update({Constant.ARTICLE_KEY: cur_id, Constant.PARAGRAPH_KEY: cur_id + '.0', Constant.SUB_PARAGRAPH_KEY: cur_id + '.0.0'})
        sys.stdout.write('Article: {} [{}].\n'.format(cur_id, text_h))
        update_index(index, article.mask, None, None, None)
    else:
        # Paragraphs
        if cur_type == Constant.PARAGRAPH_KEY:
            paragraph = Element(cur_id, cur_mask, text, text_h,  html, styles)
            sub_paragraph = sub_sub_paragraph = Element()
            prevs.update({Constant.PARAGRAPH_KEY: cur_id, Constant.SUB_PARAGRAPH_KEY: cur_id + '.0'})
            sys.stdout.write('Paragraph: {} [{}].\n'.format(cur_id, text_h))
            update_index(index, article.mask, paragraph.mask, None, None)

        # Sub-Paragraphs
        elif cur_type == Constant.SUB_PARAGRAPH_KEY:
            sub_paragraph = Element(cur_id, cur_mask, text, text_h, html, styles)
            sub_sub_paragraph = Element()
            prevs.update({Constant.SUB_PARAGRAPH_KEY: cur_id})
            sys.stdout.write('Sub-Paragraph: {} [{}].\n'.format(cur_id, text_h))
            update_index(index, article.mask, paragraph.mask, sub_paragraph.mask, None)

        # Sub-sub-Paragraphs or inner elements
        elif cur_type == Constant.SUB_SUB_PARAGRAPH_KEY:
            sub_sub_paragraph = Element(cur_id, cur_mask, text, text_h, html, styles)
            prevs.update({Constant.SUB_SUB_PARAGRAPH_KEY: cur_id})
            sys.stdout.write('Sub-sub-Paragraph: {} [{}].\n'.format(cur_id, text_h))
            update_index(index, article.mask, paragraph.mask, sub_paragraph.mask, sub_sub_paragraph.mask)

        # Inner elements <p>, <div>, <table>
        else:
            # To include paragraphs without article (in the header)
            if article.__id__ == '0':
                update_index(index, '', None, None, None)

            if element.tag == 'div' or element.tag == 'table':
                table = element.find('.//table')
                text, html = extract_table(table) if table is not None else extract_table(element)

    key = Item.format_key(article, paragraph, sub_paragraph, sub_sub_paragraph)
    item = data.get(key) if data.get(key) else Item(article, paragraph, sub_paragraph, sub_sub_paragraph)
    item.add(text, text_h, html, styles)

    data.update({key: item})

    return index, data, article, paragraph, sub_paragraph, sub_sub_paragraph


def extract(locale, from_doc, to_doc):
    """
    Extracts a (HTML) document's text, and provides a sort of structure to hold the Contract's article, paragraph, sub_paragraph,
    so as to be able to "group" text paragraphs inside them; uses <extract_content> and <extract_table> to build every item (higher
    tag level) and add it to a dict. The whole process receives 2 documents (one as base, the other, as the updated version) a returns a
    dictionary (per document) with the following format:
        {
            index   : list(str)
            base: {
                item_1: Item,
                item_2: Item,
                ...
            },
            updated: {
                item_1: Item,
                item_2: Item,
                ...
            }
        }

    :param str locale: Language/Country to configure headers.
    :param str from_doc: Filename of the base document
    :param str to_doc: Filename of the updated document
    :return: A dict containing both documents (base and updated), plus the index list of the merged documents.
    """

    try:
        parser = etree.HTMLParser(remove_comments=True)

        tree_b = etree.parse(StringIO(from_doc), parser)
        from_tree = tree_b.findall('body/*')

        tree_u = etree.parse(StringIO(to_doc), parser)
        to_tree = tree_u.findall('body/*')

        header = [h.lower() for h in select_header(locale, Constant.ARTICLE_KEY)]
        header = ['articolo', 'article', 'artikel', 'articulo'] if not header else header  # To allow start the process without locale

        # Master Index of elements in both documents
        index = dict()

        sys.stdout.write('Extracting data from <base_contract>.\n')
        style_b = get_html(tree_b.find('head/style'))
        index, base_dict = extract_content(from_tree, index, header, style_b)

        sys.stdout.write('Extracting data from <updated_contract>.\n')
        style_u = get_html(tree_u.find('head/style'))
        index, updated_dict = extract_content(to_tree, index, header, style_u)

        return {'index': index_tostring(index), 'base': base_dict, 'updated': updated_dict}
    except Exception as e:
        sys.stderr.write('{}\n'.format(e))
        raise e


if checkbot.app.config['EXPOSE_ALL']:

    @checkbot.app.route("/checkbot/extract/<locale>", methods=['POST'])
    def _extract(locale):
        if not request.form:
            raise IOError('Malformed POST body.')

        from_doc = request.form['base_contract']
        to_doc = request.form['updated_contract']

        return jsonify(extract(locale, from_doc, to_doc)), 200
