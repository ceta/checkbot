import checkbot
import requests
import sys

from checkbot.common.utils import get_file_extension
from checkbot.comparator.core import compare
from checkbot.extractor.core import extract
from checkbot.renderer.core import render
from flask import request, jsonify


def upload(req):
    """
    Handles the 2 files upload (base and updated) needed to start the entire process.

    :param flask.request req: Request
    :return: A tuple containing the base document and the updated document
    """
    if 'base_contract' not in req.files:
        raise FileNotFoundError('Expected file "{}" not found.'.format('base_contract'))

    if 'updated_contract' not in req.files:
        raise FileNotFoundError('Expected file "{}" not found.'.format('updated_contract'))

    base_contract = req.files['base_contract']
    updated_contract = req.files['updated_contract']

    if not base_contract.filename or not updated_contract:
        raise FileNotFoundError('No selected file.')

    if get_file_extension(base_contract.filename) not in checkbot.app.config['SUPPORTED_TYPES'] and \
            get_file_extension(updated_contract.filename) not in checkbot.app.config['SUPPORTED_TYPES']:
        raise IOError('Not supported file extension.')

    return base_contract, updated_contract


def convert(contract, filename=''):
    """
    Calls external "Converter" service on <contract>, return an .html version of the document.

    :param file contract: Contract file (in any supported type) to be converted into HTML.
    :return: Converted file.
    """
    if not contract and filename:
        if filename == '03. Offer_Mismatch_P0 - WCG.DOC':
            base_filename = '03. Offer_Mismatch_P0 - User.doc'
        elif filename == '07. EUROFOIL 2020 2021 WCG.DOC':
            base_filename = '07. EUROFOIL 2020 2021 User.doc'
        elif filename == 'CCB - BE EN - 001 - OF1.doc':
            base_filename = 'CCB - BE EN - 001.doc'
        elif filename == 'CCB - BE EN - 002 - MaxACQ.doc':
            base_filename = 'CCB - BE EN - 002.doc'
        elif filename == 'CCB - BE EN - 006 - v5 - QCQ.doc':
            base_filename = 'CCB - BE EN - 006 - v4.doc'
        elif filename == 'CCB - FR EN - 012 - v1 - Varie.doc':
            base_filename = 'CCB - FR EN - 012 - v1.doc'
        elif filename == 'CCB - 001 - Post.doc':
            base_filename = 'CCB - 001 - Originale.doc'
        elif filename == '3M - 2019-2020 (24 mois).doc':
            base_filename = 'OE 180717_3M - 2019-2020 (24 mois)-CONTR.DOC'
        elif filename == 'TERMICA MILAZZO GY18 WCG.DOC':
            base_filename = 'TERMICA MILAZZO GY18 Utente.doc'
        elif filename == 'updated.doc':
            base_filename = 'base.doc'
        else:
            base_filename = filename
        contract = open('C:\\Users\\a.lopezgonzalez\\Documents\\Workspaces\\python-workspaces\\checkbot\\data\\' + base_filename, 'rb')

    filename = filename if filename else contract.filename
    req = requests.post(checkbot.app.config['CONVERTER_URL'], files={'file': (filename, contract.read())})
    req.raise_for_status()

    return req


def build(result):
    """
    Formats the object received from the comparison service, so as to be used directly in any client. The output then
    is composed as follows:

    {
        (dict) 'data'   : List of elements containing differences.
                {
                    (int) 'id'                  : Element's sequential id.
                    (str) 'article'             : Article id, if any.
                    (str) 'paragraph'           : Paragraph id, if any.
                    (str) 'sub_paragraph'       : Sub-paragraph id, if any.
                    (str) 'base_text'           : HTML representation of the base/standard contract, with highlights, if any.
                    (str) 'base_text_style'     : Base element's style.
                    (str) 'updated_text'        : HTML representation of the updated/modified contract, with highlights, if any.
                    (str) 'updated_text_style'  : Updated element's style.
                    (str) 'contract_item'       : Contractual Item, if configured.
                    (str) 'error_type'          : Error type (Wrong value, Missing value).
                }
    }
    :param dict result: Comparison result object as dict.
    :return: Formatted output.
    """
    html = dict()

    data = result.get('data')

    if data:
        rows = list()
        for i, element in enumerate(data):
            root = element.get('base') if element.get('base') else element.get('updated')

            base = element.get('base')
            updated = element.get('updated')
            error = element.get('error')

            if checkbot.app.config['EXPOSE_ALL']:
                article = root.get('item').get('article').get('text') if root and root.get('item') and root.get('item').get(
                        'article') else ''
                paragraph = root.get('item').get('paragraph').get('text') if root and root.get('item') and root.get(
                        'item').get('paragraph') else ''
                sub_paragraph = root.get('item').get('sub_paragraph').get('text') if root and root.get('item') and root.get(
                        'item').get('sub_paragraph') else ''

                base_text = base.get('element').get('html') if base and base.get('element') else ''
                base_text_style = ''.join(base.get('element').get('styles')) if base and base.get('element') else ''
                updated_text = updated.get('element').get('html') if updated and updated.get('element') else ''
                updated_text_style = ''.join(updated.get('element').get('styles')) if updated and updated.get('element') else ''
                contract_item = error.get('contract_item') if updated and error.get('contract_item') else ''
                error_type = error.get('error_type') if updated and error.get('error_type') else ''
            else:
                article = root.get('item').article.text if root and root.get('item') and root.get(
                    'item').article else ''
                paragraph = root.get('item').paragraph.text if root and root.get('item') and root.get(
                    'item').paragraph else ''
                sub_paragraph = root.get('item').sub_paragraph.text if root and root.get('item') and root.get(
                    'item').sub_paragraph else ''

                base_text = base.get('element').html if base and base.get('element') else ''
                base_text_style = ''.join(base.get('element').styles) if base and base.get('element') else ''
                updated_text = updated.get('element').html if updated and updated.get('element') else ''
                updated_text_style = ''.join(updated.get('element').styles) if updated and updated.get('element') else ''
                contract_item = error.contract_item
                error_type = error.error_type

            rows.append({'id': i+1,
                         'article': article,
                         'paragraph': paragraph,
                         'sub_paragraph': sub_paragraph,
                         'base_text': base_text,
                         'base_text_style': base_text_style,
                         'updated_text': updated_text,
                         'updated_text_style': updated_text_style,
                         'contract_item': contract_item,
                         'error_type': error_type})

        html.update({'data': rows})

    return html


if checkbot.app.config['SERVER_APP']:

    @checkbot.app.route('/checkbot', defaults={'locale': None}, methods=['GET', 'POST'])
    @checkbot.app.route('/checkbot/<locale>', methods=['GET', 'POST'])
    def process(locale):
        """
        Starts the whole process, thus orchestrates all processes needed i.e. upload, convert, extract and compare.
        * Additionally, calls the render service, to display an HTML with the result.

        :param str locale: Language/Country to properly select params, enums, tables and more.
        :return: An HTML page with all deltas highlighted, if any.
        """
        try:
            if request.method == 'POST':
                sys.stdout.write('Process started.\n')
                base_contract, updated_contract = upload(request)

                # Calling converter on base contract
                sys.stdout.write('Calling service <convert> on file <{}>\n'.format(base_contract.filename))
                # base = convert(None, filename=base_contract.filename).content
                base = convert(base_contract).content
                sys.stdout.write('Sub-process <convert> finished.\n')

                # Calling converter on updated contract
                sys.stdout.write('Calling service <convert> on file <{}>\n'.format(updated_contract.filename))
                updated = convert(updated_contract).content
                sys.stdout.write('Sub-process <convert> finished.\n')

                # If all processes are deployed as micro-services
                if checkbot.app.config['EXPOSE_ALL']:
                    # Calling Extractor on both files
                    sys.stdout.write('Calling service <extract> on files <{}> and <{}>\n'.format(base_contract.filename, updated_contract.filename))
                    payload = {'base_contract': base, 'updated_contract': updated}
                    req = requests.post(checkbot.app.config['EXTRACTOR_URL'].format(locale), data=payload)
                    sys.stdout.write('Sub-process <extract> finished.\n')
                    req.raise_for_status()

                    # Calling Comparator on both files
                    sys.stdout.write('Calling service <compare> on files <{}> and <{}>\n'.format(base_contract.filename, updated_contract.filename))
                    req = requests.post(checkbot.app.config['COMPARATOR_URL'].format(locale), json=req.json())
                    sys.stdout.write('Sub-process <compare> finished.\n')
                    req.raise_for_status()

                    # Calling Formatter on result
                    sys.stdout.write('Formatting output\n')
                    formatted_data = build(req.json())
                    sys.stdout.write('Formatting finished.\n')

                    if checkbot.app.config['RENDER']:
                        # Calling HTML Renderer on comparison result
                        sys.stdout.write('Calling service <render>\n')
                        req = requests.post(checkbot.app.config['RENDERER_URL'], data=formatted_data)
                        sys.stdout.write('Sub-process <render> finished.\n')
                        req.raise_for_status()
                        result = req.text
                    else:
                        result = formatted_data
                else:
                    base = base.decode()
                    updated = updated.decode()

                    # Calling Extractor on both files
                    sys.stdout.write('Calling service <extract> on files <{}> and <{}>\n'.format(base_contract.filename, updated_contract.filename))
                    extracted_data = extract(locale, base, updated)
                    sys.stdout.write('Sub-process <extract> finished.\n')

                    # Calling Comparator on both files
                    sys.stdout.write('Calling service <compare> on files <{}> and <{}>\n'.format(base_contract.filename, updated_contract.filename))
                    compared_data = compare(locale, extracted_data)
                    sys.stdout.write('Sub-process <compare> finished.\n')

                    # Calling Formatter on result
                    sys.stdout.write('Formatting output\n')
                    formatted_data = build(compared_data)
                    sys.stdout.write('Formatting finished.\n')

                    if checkbot.app.config['RENDER']:
                        # Calling HTML Renderer on comparison result
                        sys.stdout.write('Calling service <render>\n')
                        result = render(formatted_data)
                        sys.stdout.write('Sub-process <render> finished.\n')
                    else:
                        result = jsonify(formatted_data)

                sys.stdout.write('Process finished.\n')
                return result, 200
            return """
                <!DOCTYPE html>
                <title>Upload new File</title>
                <h1>Upload new File</h1>
                <form action="" method="POST" enctype="multipart/form-data">
                    <p><input type="file" name="base_contract"></p>
                    <p><input type="file" name="updated_contract"></p>
                    <input type="submit" value="Upload">
                </form>
                """, 200
        except Exception as e:
            sys.stderr.write('{}\n'.format(e))
            return """{}""".format(e), 500


    @checkbot.app.route('/aliveness/ping', methods=['GET'])
    def ping():
        """
        URL to test if the server has been started properly.

        :return: Ok, if alive.
        """
        return 'OK'
