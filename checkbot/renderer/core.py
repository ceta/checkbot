import checkbot
import json
import sys

from flask import request


def render(result):
    """
    Renders an HTML page with the final result of the process. This service ONLY output the result in a table, with the
    styles provided for each document.

    :param result: Dictionary with plain data (already processed).
    :return: HTML text (ready to be saved/displayed).
    """
    try:
        data = result.get('data')

        script = '<script>\n      function resizeIframe(obj) {\n        '
        script += 'obj.style.height = obj.contentWindow.document.body.scrollHeight + 20 + \'px\';\n        '
        script += 'obj.style.width = obj.contentWindow.document.body.scrollWidth + \'px\';\n      '
        script += '};\n    </script>'

        result = '<html>\n  <head>\n    <META http-equiv="Content-Type" content="text/html; charset=utf-8">\n    {}\n  </head>\n  <body>\n    <table border="1">'.format(script) + ('      <th style="text-align: center">{}</th>\n' * 7).format(
            'Article', 'Paragraph', 'Sub-Paragraph', 'Base Text', 'Updated Text', 'Contract Item', 'Error')

        if data:
            for element in data:
                i_frame = '<iframe srcdoc="{} {}" frameborder="0" scrolling="no" onload="resizeIframe(this)"></iframe>'
                b_text = ('\n        <td>' + i_frame + '</td>').format('<style>{}</style>'.format(element.get('base_text_style')).replace('"', '&#34;').replace('\'', '&#39;'), element.get('base_text').replace('"', '&#34;').replace('\'', '&#39;'))
                u_text = ('\n        <td>' + i_frame + '</td>').format('<style>{}</style>'.format(element.get('updated_text_style')).replace('"', '&#34;').replace('\'', '&#39;'), element.get('updated_text').replace('"', '&#34;').replace('\'', '&#39;'))
                result += ('\n      <tr>' + ('\n        <td>{}</td>' * 3) + '{}{}' + ('\n        <td>{}</td>' * 2) + '\n      </tr>')\
                    .format(element.get('article'), element.get('paragraph'), element.get('sub_paragraph'), b_text, u_text, element.get('contract_item'), element.get('error_type'))
        else:
            result += '      <tr><td colspan="7" style="text-align: left">Nothing to show</td><tr>'

        result += '\n    </table>\n  </body>\n</html>'

        return result
    except Exception as e:
        sys.stderr.write('{}\n'.format(e))
        raise e


if checkbot.app.config['EXPOSE_ALL']:

    @checkbot.app.route('/render', methods=['POST'])
    def _render():
        """
        Decorator for FLASK micro-services
        """
        result = json.loads(request.data)

        return render(result), 200
