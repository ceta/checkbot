import difflib
import checkbot
import sys

from checkbot.common.domain import Element, Error
from checkbot.common.utils import Constant, get_value
from checkbot.comparator.utils import *
from flask import request, jsonify


def clean_differences(differences, from_array, to_array, escaped_characters):
    """
    Returns a list of deltas purging those in which the difference is an escaped character.

    :param list differences: Raw list (without considering escaped characters) with deltss obtained from difflib.ndiff
    :param list from_array: Original (without escaping) base paragraph lines.
    :param list to_array: Original (without escaping) updated paragraph lines.
    :param str escaped_characters: A string containing all escaped characters.
    :return: List
    """
    # Escaping all configured characters, and formatting the result, i.e replacing the diff output, when the
    # specific character is an escaped character
    delta = escape(differences, escaped_characters)

    # Unfolding blank spaces wrt the original text.
    delta = clean(delta, from_array, to_array)

    delta += ['', '', '']
    clean_result = []

    i = 0

    while i < len(delta) - 1:
        if delta[i].startswith('- '):

            # 1. If there is an incremental change w.r.t the base paragraph
            if delta[i + 1].startswith('? ') or (
                    delta[i + 1].startswith('+ ') and delta[i + 2].startswith('? ')):  # Changes among paragraphs

                # Incremental change ONLY in the updated paragraph
                if delta[i + 1].startswith('+ '):
                    if delta[i + 2][2:].strip():
                        clean_result.append(delta[i])
                        clean_result.append(delta[i+1])
                        clean_result.append(delta[i+2])
                    i += 3 if delta[i + 2].startswith('? ') else 2

                # Incremental change ONLY in the base paragraph
                elif not delta[i+3].startswith('?'):
                    if delta[i+1][2:].strip():
                        clean_result.append(delta[i])
                        clean_result.append(delta[i+1])
                        clean_result.append(delta[i+2])

                    i += 4 if delta[i + 3].startswith('? ') else 3

                # Incremental change in both paragraphs
                else:
                    # If there is a delta in the base paragraph, we add base's line and output
                    if delta[i + 1][2:].strip():
                        clean_result.append(delta[i])
                        clean_result.append(delta[i + 1])
                    # If there is a delta in the updated paragraph, we add base's line
                    elif delta[i + 3][2:].strip():
                        clean_result.append(delta[i])

                    # If there is a delta in the updated paragraph, we add updated's line and output
                    if delta[i + 3][2:].strip():
                        clean_result.append(delta[i + 2])
                        clean_result.append(delta[i + 3])
                    # If there is a delta in the base paragraph, we add base's line
                    elif delta[i + 1][2:].strip():
                        clean_result.append(delta[i + 2])

                    i += 4 if delta[i + 3].startswith('? ') else 3

            # The line is not present in the updated paragraph
            else:
                if delta[i][2:].strip():
                    clean_result.append(delta[i])
                i += 1

        # The line is not present in the base paragraph
        elif delta[i].startswith('+ ') and not delta[i + 1].startswith('? '):
            if delta[i][2:].strip():
                clean_result.append(delta[i])
            i += 1

        else:  # Should not reach here
            i += 1

    return clean_result


def diff(from_item, to_item, params, tables, enums, ignored_cols, escaped_characters, error_messages):
    """
    Returns a list of tuples containing all differences present in the text, where:
        (0): Element from the base item (with its position within the item)
        (1): Element from the updated item (with its position within the item)
        (2): Generated error

    :param checkbot.common.domain.Item from_item: Item to evaluate from the base document.
    :param checkbot.common.domain.Item to_item: Item to evaluate from the updated document.
    :param dict params: Dictionary with params to evaluate.
    :param dict tables: Dictionary with tables to evaluate.
    :param dict enums: Dictionary with enums to evaluate.
    :param dict ignored_cols: Dictionary with ignored columns to consider.
    :param str escaped_characters:
    :param dict error_messages: Dictionary with all error messages filtered out by the locale.
    :return: A list of tuples.
    """
    diff_list = list()

    # Fix ndiff behaviour, when many blank spaces are found, may consider the both paragraphs different,
    # causing entrances in the delta array, that should not be there, thus, the diff process should escape
    # multiple blank spaces.
    from_lines = format_small_lines(from_item.to_array(escaped=True, ignored_cols=ignored_cols), checkbot.app.config['MINIMUM_PARAGRAPH_SIZE'])
    to_lines = format_small_lines(to_item.to_array(escaped=True, ignored_cols=ignored_cols), checkbot.app.config['MINIMUM_PARAGRAPH_SIZE'])

    # To keep the content as it is (without escaping).
    from_array = from_item.to_array(escaped=False, ignored_cols=ignored_cols)
    to_array = to_item.to_array(escaped=False, ignored_cols=ignored_cols)

    # .ndiff Returns a list of strings
    # Lines prefixed with - indicate that they were in the first sequence, but not the second.
    # Lines prefixed with + were in the second sequence, but not the first.
    # If a line has an incremental difference between versions, an extra line prefixed with ? is used to highlight the change within the new version.
    # If a line has not changed, it is printed with an extra blank space on the left column so that it it lines up with the other lines that may have differences.

    ndiff = [x for x in difflib.ndiff(from_lines, to_lines)]

    delta = clean_differences(ndiff, from_array, to_array, escaped_characters)

    # If there exists any difference in the updated document
    if delta:
        delta += ['* ', '* ', '* ']  # to look-forward patterns
        i = 0

        while i < len(delta) - 1:

            if delta[i].startswith('- '):

                # If a line/word/phrase has changed or is not present should be in one of the following scenarios

                # 1. If there is an incremental change w.r.t the base paragraph
                if delta[i + 1].startswith('? ') or (
                        delta[i + 1].startswith('+ ') and delta[i + 2].startswith('? ')):  # Changes among paragraphs

                    # Incremental change ONLY in the updated paragraph
                    if delta[i + 1].startswith('+ '):
                        e1, i_e1 = from_item.search(delta[i][2:], ignored_cols)
                        e2, i_e2 = to_item.search(delta[i + 1][2:], ignored_cols)

                        if isinstance(e1.text, list) and isinstance(e2.text, list):
                            contract_item = search_tables(e1.text, tables, error_messages)
                            y = table_error_positions(delta[i + 2], delta[i + 1][2:], e2.text, ignored_cols)
                            diff_list.append(((e1, i_e1), (e2, i_e2), Error(contract_item, error_messages.get(Constant.ERROR_WRONG_VALUE), [], y)))
                        else:
                            y = text_error_positions(delta[i + 2], delta[i + 1][2:])
                            missing_parameters = search_params(delta[i][2:], delta[i + 1][2:], params, enums, error_messages)

                            if missing_parameters:
                                for error in missing_parameters:
                                    diff_list.append(((e1, i_e1), (e2, i_e2), Error(error[0], error[1], [], y)))
                            else:
                                diff_list.append(((e1, i_e1), (e2, i_e2), Error(error_messages.get(Constant.CONTRACT_ITEM_NON_AVAILABLE), error_messages.get(Constant.ERROR_WRONG_VALUE), [], y)))

                        i += 3 if delta[i + 2].startswith('? ') else 2

                    # Incremental change in the base paragraph and/or the updated paragraph
                    else:
                        e1, i_e1 = from_item.search(delta[i][2:], ignored_cols)
                        e2, i_e2 = to_item.search(delta[i + 2][2:], ignored_cols)

                        if isinstance(e1.text, list) and isinstance(e2.text, list):
                            contract_item = search_tables(e1.text, tables, error_messages)

                            x = table_error_positions(delta[i + 1], delta[i][2:], e1.text, ignored_cols)
                            y = table_error_positions(delta[i + 3], delta[i + 2][2:], e2.text, ignored_cols)
                            diff_list.append(((e1, i_e1), (e2, i_e2), Error(contract_item, error_messages.get(Constant.ERROR_WRONG_VALUE), x, y)))
                        else:
                            missing_parameters = search_params(delta[i][2:], delta[i + 2][2:], params, enums, error_messages)

                            x = text_error_positions(delta[i + 1], delta[i][2:])
                            y = text_error_positions(delta[i + 3], delta[i + 2][2:])

                            if missing_parameters:
                                for error in missing_parameters:
                                    diff_list.append(((e1, i_e1), (e2, i_e2), Error(error[0], error[1], x, y)))
                            else:
                                diff_list.append(((e1, i_e1), (e2, i_e2), Error(error_messages.get(Constant.CONTRACT_ITEM_NON_AVAILABLE), error_messages.get(Constant.ERROR_WRONG_VALUE), x, y)))

                        i += 4 if delta[i + 3].startswith('? ') else 3

                # The line is not present in the updated paragraph
                else:
                    e1, i_e1 = from_item.search(delta[i][2:], ignored_cols)

                    if isinstance(e1.text, list):
                        contract_item = search_tables(e1.text, tables, error_messages)
                        e2, i_e2 = to_item.search('\t'.join(e1.text[0]), ignored_cols)
                        x = table_error_positions(None, delta[i][2:], e1.text, ignored_cols)
                        diff_list.append(((e1, i_e1), (e2, i_e2), Error(contract_item, error_messages.get(Constant.ERROR_MISSING_VALUE), x, [])))
                    else:
                        missing_parameters = search_params(delta[i][2:], '', params, enums, error_messages)
                        x = text_error_positions(None, e1.text)

                        if missing_parameters:
                            for error in missing_parameters:
                                diff_list.append(((e1, i_e1), (Element(), -1), Error(error[0], error[1], x, [])))
                        else:
                            diff_list.append(((e1, i_e1), (Element(), -1), Error(error_messages.get(Constant.CONTRACT_ITEM_NON_AVAILABLE), error_messages.get(Constant.ERROR_MISSING_VALUE), x, [])))

                    i += 1

            # The line is not present in the base paragraph
            elif delta[i].startswith('+ ') and not delta[i + 1].startswith('? '):
                e2, i_e2 = to_item.search(delta[i][2:], ignored_cols)  # format for tables

                if isinstance(e2.text, list):
                    contract_item = search_tables(e2.text, tables, error_messages)
                    e1, i_e1 = from_item.search('\t'.join(e2.text[0]), ignored_cols)
                    y = table_error_positions(None, delta[i][2:], e2.text, ignored_cols)
                    diff_list.append(((e1, i_e1), (e2, i_e2), Error(contract_item, error_messages.get(Constant.ERROR_MISSING_VALUE), [], y)))
                else:
                    y = text_error_positions(None, e2.text)
                    diff_list.append(((Element(), -1), (e2, i_e2), Error(error_messages.get(Constant.CONTRACT_ITEM_NON_AVAILABLE), error_messages.get(Constant.ERROR_MISSING_VALUE), [], y)))

                i += 1

            else:  # Should not reach here
                i += 1

    return diff_list


def compare(locale, data):
    """
    Compares 2 documents, the base and updated contract; uses <diff> to perform evaluation item by item, so as to avoid
    comparison at line-level and lose context (if a line or text paragraph is missing in one of the documents, could trigger
    an unexpected behaviour). Returns a list of dictionaries with the following format:
        {
            [
                base: {
                    item: Item,
                    element: Element or Item,
                    index: int
                },
                updated: {
                    item: Item,
                    element: Element or Item,
                    index: int
                },
                error:  {
                    contract_item: str,
                    error_type: str
                }
            ]
        }

    :param str locale: Language/Country to configure params, enums, tables and more.
    :param dict data: Dictionary containing the html, head, base and updated extracted content.
    :return: A dictionary containing the deltas from the base and updated documents.
    """
    _params, _tables, _enums, _ignored_cols, escaped_characters, error_messages = get_db_data(locale)
    escaped_characters = escaped_characters if escaped_characters else ' \t\r\n'

    try:
        index = data.get('index')
        base_dict = data.get('base')
        updated_dict = data.get('updated')

        summary = list()

        for item in index:
            base = base_dict.get(item)
            updated = updated_dict.get(item)

            if base and updated:
                element = base.sub_sub_paragraph if base.sub_sub_paragraph else (
                    base.sub_paragraph if base.sub_paragraph else (base.paragraph if base.paragraph else base.article))

                # Filtering configurations for the current item
                params = get_value(_params, element.text)
                tables = get_value(_tables, element.text)
                enums = get_value(_enums, element.text)
                ignored_cols = get_value(_ignored_cols, element.text)
                updated_text = diff(base, updated, params, tables, enums, ignored_cols, escaped_characters, error_messages)

                if updated_text:
                    # Sentences with differences
                    for updated_line in updated_text:
                        base_e = Element(updated_line[0][0].__id__, updated_line[0][0].mask, updated_line[0][0].text, None, parse_html(updated_line[0][0], updated_line[2].x), updated_line[0][0].styles)  # To not update the original element
                        updated_e = Element(updated_line[1][0].__id__, updated_line[1][0].mask, updated_line[1][0].text, None, parse_html(updated_line[1][0], updated_line[2].y), updated_line[1][0].styles)  # To not update the original element

                        r_base = {'item': base, 'element': base_e, 'index': updated_line[0][1]}
                        r_updated = {'item': updated, 'element': updated_e, 'index': updated_line[1][1]}
                        summary.append({'base': r_base, 'updated': r_updated, 'error': updated_line[2]})
            elif not updated:
                # Item not present in the updated document
                base.html = ''
                for e in base.content:
                    e.html = parse_html(e, [(0, len(e.text))])
                    base.html += '<br />' + e.html

                r_base = {'item': base, 'element': base, 'index': 0}
                error = Error(error_messages.get(Constant.CONTRACT_ITEM_NON_AVAILABLE), error_messages.get(Constant.ERROR_MISSING_VALUE), None, None)
                summary.append({'base': r_base, 'updated': None, 'error': error})

            else:
                # Item not present in the base document
                updated.html = ''
                for e in updated.content:
                    e.html = parse_html(e, [(0, len(e.text))])
                    updated.html += '<br />' + e.html

                r_updated = {'item': updated, 'element': updated, 'index': 0}
                error = Error(error_messages.get(Constant.CONTRACT_ITEM_NON_AVAILABLE), error_messages.get(Constant.ERROR_MISSING_VALUE), None, None)
                summary.append({'base': None, 'updated': r_updated, 'error': error})

        return {'data': summary}
    except Exception as e:
        sys.stderr.write('{}\n'.format(e))
        raise e


if checkbot.app.config['EXPOSE_ALL']:

    @checkbot.app.route('/checkbot/compare/<locale>', methods=['POST'])
    def _compare(locale):
        """
        Decorator for FLASK micro-services
        """
        data = request.get_json()

        return jsonify(compare(locale, data)), 200
