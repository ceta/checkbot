import re

from checkbot.common.domain import Item
from checkbot.common.utils import Constant, get_pattern
from checkbot.dao.core import select_ignored_cols, select_enums, select_params, select_tables, select_escaped_characters, select_error_messages


def get_db_data(locale):
    """
    Returns a tuple containing data retrieved from db with the following elements:
        (0) All params configured for the current locale.
        (1) All tables configured for the current locale.
        (2) All enums configured for the current locale.
        (3) All ignored columns configured for the current locale.
        (5) All escaped characters configured.
        (5) All error messages configured for the current locale.

    :param str locale: Locale used to filter out db data.
    :return: Tuple with data filtered by locale.
    """
    return select_params(locale), select_tables(locale), select_enums(locale), select_ignored_cols(locale), select_escaped_characters(), select_error_messages(locale)


def search_params(from_line, to_line, params, enums, error_messages):
    """
    Returns a list of errors with all the missing/changed parameters and their respective error description.

    :param str from_line: Line from the base document.
    :param str to_line: Line from the updated document.
    :param dict params: Parameters to be evaluated in both lines.
    :param dict enums: Enumerations to be evaluated in both lines.
    :param dict error_messages: Dictionary with all error messages filtered out by the locale.
    :return: A list of errors, if any.
    """
    from_line = from_line.lower()
    to_line = to_line.lower()
    missing_parameters = []

    if params:

        for key, value in params.items():
            pattern = re.compile(get_pattern(value.replace(' ', ''), enums))
            f_value = pattern.search(from_line.replace(' ', ''))

            if f_value:  # If the parameter is in the current line on the base item
                t_value = pattern.search(to_line.replace(' ', ''))

                if t_value:  # if the parameter is in the current line on the updated item
                    if f_value.group(1) != t_value.group(1):  # if the values have changed
                        missing_parameters.append((key, error_messages.get(Constant.ERROR_WRONG_VALUE)))
                else:  # if not present, add an error
                    missing_parameters.append((key, error_messages.get(Constant.ERROR_MISSING_VALUE)))

    return missing_parameters


def search_tables(from_table, tables, error_messages):
    """
    Evaluates if the current table has been configured as a contract item.

    :param list from_table: A list-like representation of an HTML table.
    :param dict tables: Tables configured as contract items (from db).
    :param dict error_messages: Dictionary with all error messages filtered out by the locale.
    :return: An error message if any.
    """
    for key, value in tables.items():
        pattern = get_pattern(value)
        for col in from_table[0]:
            col = re.sub(' ', '', col.lower())
            if re.compile(pattern).search(col):
                return key

    return error_messages.get(Constant.CONTRACT_ITEM_NON_AVAILABLE)


def text_error_positions(output, line):
    """
    Returns a list with the positions in the text (line) that have changed.

    :param str output: Output generated from difflib e.g ?  ^^   ^^ ^.
    :param str line: Line that has generated the <output>.
    :return: List of index tuples (from, to).
    """
    pos = list()

    if not output:
        return [(0, len(line if line else ''))]
    elif output.startswith('? '):
        output = '{} '.format(output[2:])
        limit = output.rfind(output[len(output.rstrip()) - 1])  # last non-space character
        y = 0

        while y <= limit:
            non_space_character = output[y:][len(output[y:]) - len(output[y:].lstrip())]
            x = y + output[y:].find(non_space_character)
            y = x + output[x + 1:].find(' ') + 1
            pos.append((x, y))

    return pos


def table_error_positions(output, line, table, ignored_cols):
    """
    Returns a list with the positions in the text (line) that have changed. It considers the positions
    wrt to the entire table, and not line by line, as when it is evaluated.

    :param str output: Output generated from difflib e.g ? ^^   ^^ ^.
    :param str line: Line that has generated the <output>.
    :param list table: Table the <line> belongs to.
    :param dict ignored_cols: Dictionary with columns to skip.
    :return: List of indices (from, to).
    """
    filtered_table = Item.filter_table(table, ignored_cols)
    formatted_table = ['\t'.join(row) for row in table]

    length = 0

    for i, row in enumerate(table):
        filtered_row = filtered_table[i].split('\t')

        if line == filtered_table[i]:
            # To compute the diff positions considering the whole table (including the ignored columns)
            if output:
                x = 0
                formatted_output = output[0:2]

                for col in row:
                    if col not in filtered_row:
                        formatted_output += ' ' * len(col + '\t')
                    else:
                        formatted_output += output[2:][x:x + len(col + '\t')]
                        x += len(col + '\t')

                output = formatted_output

            pos = text_error_positions(output, formatted_table[i])

            return [(length + p[0], length + p[1]) for p in pos]

        length += len(formatted_table[i] + '\n')

    return [(0, length)]


def format_small_lines(lines, size=225):
    """
    Returns a formatted list of lines, in which, if the line lenght is lower than 225 (by default), it will be completed
    with blank spaces.

    Explanation: When difflib.ndiff outputs the deltas, if the lines being evaluated differ in roughly 40% the output
    will be both, missing (-) and new (+) lines (for the base and updated text respectively). In checkbot this will lead to have
    "Missing Value" errors instead of "Wrong Values" errors, this is particularly obvious when the lines being compared
    are very short, thus, a white space filling up to 225 length is done (white spaces are not considered during post-
    process of the comparison result).

    :param list(str) lines: Text paragraphs to be formatted.
    :param int size: Magic number used to consider a paragraph small or not.
    :return: A list with formatted lines.
    """
    result = list()

    for line in lines:
        blank_spaces = size - len(line) if size > len(line) else 0
        result.append(line + (blank_spaces * ' ') + '\u200b')

    return result


def unfold(unfolded_line, folded_line, output):
    """
    Returns the correspondent unfolded output. When the basic comparison is done, multiple white spaces needs to be folded
    to avoid inconsistent behaviours, however, for the final result, the original line i.e that with multiple spaces, needs
    to be considered, for highlighting the results given that the element's HTML corresponds to the original text.

    :param str unfolded_line: Original text line (multiple white spaces) e.g A    bc.
    :param str folded_line: Folded text line (single white spaces) e.g A bc.
    :param str output: difflib.ndiff *folded* output e.g ?      ^^       ++   ---
    :return: An unfolded line (all original blank spaces).
    """
    words = folded_line.split(' ')

    x = y = 0

    unfolded_output = ''
    for i, word in enumerate(words):
        if i < len(words) - 1:
            nexxt = words[i+1]
            i_next = len(word) + unfolded_line[x + len(word):].find(nexxt)
            separator = unfolded_line[x + len(word):x + i_next]
            unfolded_output += '{}{}'.format(output[y:y + len(word)], (' ' * len(separator)))
            x += len(word) + len(separator)
            y += len(word) + 1
        else:
            unfolded_output += output[y:]

    return unfolded_output


def escape(differences, escaped_characters):
    """
    Filters the raw output list from difflib.ndiff by NOT considering the differences in which the delta is generated
    by a escaped character.

    :param list differences: ndiff output list (list of deltas)
    :param str escaped_characters: All characters to be escaped i.e not considered during comparison
    :return: A filtered list.
    """
    differences += ['', '', '']

    result = []

    for i in range(len(differences)):
        # Changes present in the base text and/or the updated text
        if differences[i].startswith('- ') or differences[i].startswith('+ '):
            result.append(differences[i])

            if differences[i+1].startswith('? '):
                text = differences[i]
                output = differences[i + 1].replace('\n', '').replace('\r', '').replace('\t', '')
                # If the difference is an escaped character, skip it
                o = [' ' if text[x] in escaped_characters else output[x] for x, c in enumerate(output)]
                result.append(''.join(o))
        # Text without changes
        elif differences[i].startswith(' '):
            result.append(differences[i])

    return result


def clean(differences, from_array, to_array):
    """
    Cleans the output list from difflib.ndiff by removing the lines in which there is NO deltas. This scenario happens
    when an additional cleaning/filtering process has been done e.g escape.

    :param list differences: ndiff output list.
    :param list from_array: Base contract array of elements with original text
    :param list to_array: Updated contract array of elements with original text
    :return: A filtered list.
    """
    differences += ['', '', '']
    result = []

    i = i_x = i_y = 0

    while i < len(differences):

        if differences[i].startswith('  '):
            i_x += 1
            i_y += 1
            i += 1
            continue

        # Changes in both, updated and base paragraphs
        if differences[i].startswith('- ') and differences[i + 1].startswith('? ') and \
                differences[i + 2].startswith('+ ') and differences[i + 3].startswith('? '):

            from_item = from_array[i_x]
            to_item = to_array[i_y]

            result.append('- ' + from_item)
            result.append('? ' + unfold(from_item, differences[i][2:], differences[i + 1][2:]))
            result.append('+ ' + to_item)
            result.append('? ' + unfold(to_item, differences[i + 2][2:], differences[i + 3][2:]))

            i += 4
            i_x += 1
            i_y += 1

        # Changes ONLY on the base paragraph
        elif differences[i].startswith('- ') and differences[i + 1].startswith('? ') and \
                differences[i + 2].startswith('+ '):

            from_item = from_array[i_x]
            to_item = to_array[i_y]

            result.append('- ' + from_item)
            result.append('? ' + unfold(from_item, differences[i][2:], differences[i+1][2:]))
            result.append('+ ' + to_item)

            i += 3
            i_x += 1
            i_y += 1

        # Changes ONLY on the updated paragraph
        elif differences[i].startswith('- ') and differences[i + 1].startswith('+ ') and \
                differences[i + 2].startswith('? '):

            from_item = from_array[i_x]
            to_item = to_array[i_y]

            result.append('- ' + from_item)
            result.append('+ ' + to_item)
            result.append('? ' + unfold(to_item, differences[i + 1][2:], differences[i + 2][2:]))

            i += 3
            i_x += 1
            i_y += 1

        # Missing text on the updated paragraph
        elif differences[i].startswith('- '):
            from_item = from_array[i_x]

            result.append('- ' + from_item)

            i += 1
            i_x += 1

        # Missing text on the base paragraph
        elif differences[i].startswith('+ '):
            to_item = to_array[i_y]
            result.append('+ ' + to_item)

            i += 1
            i_y += 1

        # Should not reach here
        else:

            i += 1
            i_x += 1
            i_y += 1

    return result


def parse_html(element, pos):
    """
    Receives an Element and a list of positions (pos_x, pos_y) and parses the string representation of the HTML element
    so as to highlight the correspondent text. i.e to enclose within a <mark> tag.

    :param checkbot.common.domain.ELement element: Element to be highlighted.
    :param list pos: List of positions (pos_x, pos_y) to highlight.
    :return: HTML string (with <mark> tags highlighting the deltas).
    """
    if element.html:
        content = element.html.replace('\t', '').replace('\r', '').replace('\n', '')

        if isinstance(element.text, list):
            content = re.sub('(<table.*?>)(.*?)(<tr.*?>)', r"\1\3", content.strip())
            content = re.sub('(<tr.*?>)(.*?)(<td.*?>)', r"\1\3", content.strip())
            content = re.sub('(</tr>)(.*?)(<tr.*?>)', r"\1\3", content.strip())
            content = content.replace('</td>', '</td>\t')  # To compensate extra character induced by the list conversion

        for pos_x, pos_y in pos:
            content = highlight(content, pos_x, pos_y)

        return content

    return None


def highlight(content, pos_x, pos_y):
    """
    Receives a string representation of an HTML element, and the start and end indices of the text that
    needs to be highlighted, and retrieves a string representation of the new HTML element.

    :param str content: HTML element as string.
    :param int pos_x: Text's start index.
    :param int pos_y: Text's end index.
    :return: A string with <mark> tags, if needed.
    """
    i = j = e = 0
    result = list()

    while j < len(content) - 1:
        i += content[i:].find('<') if content[i:].find('<') >= 0 else j
        j = i + content[i:].find('>') + 1
        tag = content[i:j]
        i = j + (content[j:].find('<') if content[j:].find('<') >= 0 else j)
        text = content[j:i]
        x, y = (e, e + len(text)) if text else (None, None)
        e += len(text)
        result.append((tag, text, x, y))

    inner_html = ''

    for r in result:
        tag, text, x, y = r

        if x is None:
            inner_html += tag
        else:
            if y >= pos_x >= x:
                i = pos_x - x
                j = len(text) - (y - pos_y) if pos_y <= y else len(text)
                pos_x = pos_x if pos_y <= y else y
                mark = '<mark style="background-color: yellow">{}</mark>'\
                    .format(text[i:j])\
                    if text[i:j] not in ['\n', '', '\t', '\r'] else ''  # to avoid empty marks
                inner_html += tag + text[0:i] + mark + text[j:len(text)]
            else:
                inner_html += tag + text

    return inner_html
