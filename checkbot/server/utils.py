import json

from flask.json import JSONEncoder, JSONDecoder
from checkbot.common.domain import Element, Item, Error


class AppJSONEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, Element):
            return {
                'id': obj.__id__ if obj.__id__ else None,
                'mask': obj.mask if obj.mask else None,
                'text': obj.text if obj.text else None,
                'text_h': obj.text_h if obj.text_h else None,
                'html': obj.html if obj.html else None,
                'styles': obj.styles if obj.styles else None
            }
        if isinstance(obj, Error):
            return {
                'contract_item': obj.contract_item if obj.contract_item else None,
                'error_type': obj.error_type if obj.error_type else None,
                'x': obj.x if obj.x else None,
                'y': obj.y if obj.y else None
            }
        if isinstance(obj, Item):
            return {
                'article': obj.article if obj.article else None,
                'paragraph': obj.paragraph if obj.paragraph else None,
                'sub_paragraph': obj.sub_paragraph if obj.sub_paragraph else None,
                'sub_sub_paragraph': obj.sub_sub_paragraph if obj.sub_sub_paragraph else None,
                'content': obj.content if obj.content else None,
                'html': obj.html if obj.html else None,
                'styles': obj.styles if obj.styles else None,
            }

        return super(AppJSONEncoder, self).default(obj)


class AppJSONDecoder(JSONDecoder):

    def decode(self, o):
        req = json.loads(o)
        base = AppJSONDecoder.convert(req.get('base'))
        updated = AppJSONDecoder.convert(req.get('updated'))

        return {'index': req.get('index'), 'base': base, 'updated': updated}

    @staticmethod
    def convert(document):
        result = {}

        for key, value in document.items():
            article = Element(value.get('article').get('id'), value.get('article').get('text'), value.get('article').get('html')) if value.get('article') else Element()
            paragraph = Element(value.get('paragraph').get('id'), value.get('paragraph').get('text'), value.get('paragraph').get('html')) if value.get('paragraph') else Element()
            sub_paragraph = Element(value.get('sub_paragraph').get('id'), value.get('sub_paragraph').get('text'), value.get('sub_paragraph').get('html')) if value.get('sub_paragraph') else Element()
            sub_sub_paragraph = Element(value.get('sub_sub_paragraph').get('id'), value.get('sub_sub_paragraph').get('text'), value.get('sub_sub_paragraph').get('html')) if value.get('sub_sub_paragraph') else Element()
            item = Item(article, paragraph, sub_paragraph, sub_sub_paragraph)

            for content in value.get('content'):
                item.add(content.get('text'), content.get('text_h'), content.get('html'), content.get('styles'))
                result.update({key: item})

        return result
