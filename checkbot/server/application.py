import checkbot
import sys

from checkbot.server.utils import AppJSONEncoder, AppJSONDecoder
from flask import Flask


"""
The micro-service version exposes 2 URLs (5 if the parameter expose_all is set to True):
    - GET aliveness/ping
    - POST checkbot/<locale>
    
    - POST checkbot/extract/<locale>
    - POST checkbot/compare/<locale>
    - POST checkbot/render
    
The app uses flask as a (web) micro-framework, in order to start the application the environment must be configured, to 
do so, the application needs to be installed:
    - python setup.py build_ext --inplace
    - python setup.py install

The application uses a configuration file named config.cfg (a python script with a different extension, imports or any other
python syntax can be used inside). The configuration file location is set in an environment variable named CHECKBOT_CONFIG.

    To set the environment variable:
    - On Unix: export CHECKBOT_CONFIG=/path/to/config.cfg
    - On windows: setx CHECKBOT_CONFIG /path/to/config.cfg

The configuration file contains information such the mode in which the application is going to be started, all URLs needed
to run the entire process, the supported file extension and the database location. In a production environment, the parameters
THREADED, DEBUG and TEST should be set to False.

Finally, to start the application:

    To run the application in the default WSGI container (Werkzeug):
    - On Unix: export FLASK_APP=checkbot.server.application:app
    - On windows: set FLASK_APP=checkbot.server.application:app
    - flask run
    
    To run the application using Gunicorn, use one of the following:
    - gunicorn checkbot.server.application:app
    - gunicorn -b 127.0.0.1:4000 checkbot.server.application:app -p rocket.pid -D
"""


try:
    app = Flask(__name__)

    checkbot.app = app
    checkbot.app.json_encoder = AppJSONEncoder
    checkbot.app.json_decoder = AppJSONDecoder

    # Load config file
    checkbot.app.config.from_envvar('CHECKBOT_CONFIG')

    # Expose all sub-processes as (micro) services
    if checkbot.app.config['EXPOSE_ALL']:
        import checkbot.extractor.core
        import checkbot.comparator.core
        import checkbot.renderer.core

    # Expose main (micro) service
    import checkbot.orchestrator.core

except ValueError as e:
    sys.stderr.write('{}\n'.format(e))
    raise


