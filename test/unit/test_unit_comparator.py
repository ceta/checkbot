import sys
from checkbot.comparator.utils import *


def test_get_db_data(test_client):
    from checkbot.comparator.utils import get_db_data

    params, tables, enums, ignored_cols, escaped_characters, error_messages = get_db_data('IT_it')

    assert params
    assert tables
    assert escaped_characters
    assert error_messages
    assert not enums
    assert not ignored_cols


def test_search_params():
    assert [('P0', 'Valore errato')] == search_params('The value of P0 is 3.72.', 'The value of P0 is 3.58.', {'P0': 'The value of P0 is <numeric>.'}, {}, {'M': 'Valore mancante', 'C': 'Valore errato'})


def test_search_tables(test_client):
    """
    for key, value in tables.items():
        pattern = get_pattern(value)
        for col in from_table[0]:
            col = re.sub(' ', '', col.lower())
            if re.compile(pattern).search(col):
                return key

    return error_messages.get(Constant.CONTRACT_ITEM_NON_AVAILABLE)
    """
    from checkbot.comparator.core import search_tables
    errors = {'M': 'Valore mancante', 'C': 'Valore errato'}
    tables = {'Cost for evertake': 'Sito'}
    search_tables(from_table, tables, errors)


def text_error_positions():
    """
    pos = list()

    if not output:
        return [(0, len(line if line else ''))]
    elif output.startswith('? '):
        output = '{} '.format(output[2:])
        limit = output.rfind(output[len(output.rstrip()) - 1])  # last non-space character
        y = 0

        while y <= limit:
            non_space_character = output[y:][len(output[y:]) - len(output[y:].lstrip())]
            x = y + output[y:].find(non_space_character)
            y = x + output[x + 1:].find(' ') + 1
            pos.append((x, y))

    return pos
    """
    pass


def test_table_error_positions():
    """
    filtered_table = Item.filter_table(table, ignored_cols)
    formatted_table = ['\t'.join(row) for row in table]

    length = 0

    for i, row in enumerate(table):
        filtered_row = filtered_table[i].split('\t')

        if line == filtered_table[i]:
            # To compute the diff positions considering the whole table (including the ignored columns)
            if output:
                x = 0
                formatted_output = output[0:2]

                for col in row:
                    if col not in filtered_row:
                        formatted_output += ' ' * len(col + '\t')
                    else:
                        formatted_output += output[2:][x:x + len(col + '\t')]
                        x += len(col + '\t')

                output = formatted_output

            pos = text_error_positions(output, formatted_table[i])

            return [(length + p[0], length + p[1]) for p in pos]

        length += len(formatted_table[i] + '\n')

    return [(0, length)]
    """
    pass


def format_small_lines():
    """
    result = list()
    print(size)
    for line in lines:
        blank_spaces = size - len(line) if size > len(line) else 0
        result.append(line + (blank_spaces * ' ') + '\u200b')

    return result
    """
    pass


def test_unfold():
    """
    words = folded_line.split(' ')

    x = y = 0

    unfolded_output = ''
    for i, word in enumerate(words):
        if i < len(words) - 1:
            nexxt = words[i+1]
            i_next = len(word) + unfolded_line[x + len(word):].find(nexxt)
            separator = unfolded_line[x + len(word):x + i_next]
            unfolded_output += '{}{}'.format(output[y:y + len(word)], (' ' * len(separator)))
            x += len(word) + len(separator)
            y += len(word) + 1
        else:
            unfolded_output += output[y:]

    return unfolded_output
    """
    pass


def test_escape():
    """
    differences += ['', '', '']

    result = []

    for i in range(len(differences)):
        # Changes present in the base text and/or the updated text
        if differences[i].startswith('- ') or differences[i].startswith('+ '):
            result.append(differences[i])

            if differences[i+1].startswith('? '):
                text = differences[i]
                output = differences[i + 1].replace('\n', '').replace('\r', '').replace('\t', '')
                # If the difference is an escaped character, skip it
                o = [' ' if text[x] in escaped_characters else output[x] for x, c in enumerate(output)]
                result.append(''.join(o))
        # Text without changes
        elif differences[i].startswith(' '):
            result.append(differences[i])

    return result
    """
    pass


def test_clean():
    """
    differences += ['', '', '']
    result = []

    i = i_x = i_y = 0

    while i < len(differences):

        if differences[i].startswith('  '):
            i_x += 1
            i_y += 1
            i += 1
            continue

        # Changes in both, updated and base paragraphs
        if differences[i].startswith('- ') and differences[i + 1].startswith('? ') and \
                differences[i + 2].startswith('+ ') and differences[i + 3].startswith('? '):

            from_item = from_array[i_x]
            to_item = to_array[i_y]

            result.append('- ' + from_item)
            result.append('? ' + unfold(from_item, differences[i][2:], differences[i + 1][2:]))
            result.append('+ ' + to_item)
            result.append('? ' + unfold(to_item, differences[i + 2][2:], differences[i + 3][2:]))

            i += 4
            i_x += 1
            i_y += 1

        # Changes ONLY on the base paragraph
        elif differences[i].startswith('- ') and differences[i + 1].startswith('? ') and \
                differences[i + 2].startswith('+ '):

            from_item = from_array[i_x]
            to_item = to_array[i_y]

            result.append('- ' + from_item)
            result.append('? ' + unfold(from_item, differences[i][2:], differences[i+1][2:]))
            result.append('+ ' + to_item)

            i += 3
            i_x += 1
            i_y += 1

        # Changes ONLY on the updated paragraph
        elif differences[i].startswith('- ') and differences[i + 1].startswith('+ ') and \
                differences[i + 2].startswith('? '):

            from_item = from_array[i_x]
            to_item = to_array[i_y]

            result.append('- ' + from_item)
            result.append('+ ' + to_item)
            result.append('? ' + unfold(to_item, differences[i + 1][2:], differences[i + 2][2:]))

            i += 3
            i_x += 1
            i_y += 1

        # Missing text on the updated paragraph
        elif differences[i].startswith('- '):
            from_item = from_array[i_x]

            result.append('- ' + from_item)

            i += 1
            i_x += 1

        # Missing text on the base paragraph
        elif differences[i].startswith('+ '):
            to_item = to_array[i_y]
            result.append('+ ' + to_item)

            i += 1
            i_y += 1

        # Should not reach here
        else:

            i += 1
            i_x += 1
            i_y += 1

    return result
    """
    pass


def test_parse_html():
    """
    if element.html:
        content = element.html.replace('\t', '').replace('\r', '').replace('\n', '')

        if isinstance(element.text, list):
            content = re.sub('(<table.*?>)(.*?)(<tr.*?>)', r"\1\3", content.strip())
            content = re.sub('(<tr.*?>)(.*?)(<td.*?>)', r"\1\3", content.strip())
            content = re.sub('(</tr>)(.*?)(<tr.*?>)', r"\1\3", content.strip())
            content = content.replace('</td>', '</td>\t')  # To compensate extra character induced by the list conversion

        for pos_x, pos_y in pos:
            content = highlight(content, pos_x, pos_y)

        return content

    return None
    """
    pass


def highlight():
    """
    i = j = e = 0
    result = list()

    while j < len(content) - 1:
        i += content[i:].find('<') if content[i:].find('<') >= 0 else j
        j = i + content[i:].find('>') + 1
        tag = content[i:j]
        i = j + (content[j:].find('<') if content[j:].find('<') >= 0 else j)
        text = content[j:i]
        x, y = (e, e + len(text)) if text else (None, None)
        e += len(text)
        result.append((tag, text, x, y))

    inner_html = ''

    for r in result:
        tag, text, x, y = r

        if x is None:
            inner_html += tag
        else:
            if y >= pos_x >= x:
                i = pos_x - x
                j = len(text) - (y - pos_y) if pos_y <= y else len(text)
                pos_x = pos_x if pos_y <= y else y
                mark = '<mark style="background-color: yellow">{}</mark>'\
                    .format(text[i:j])\
                    if text[i:j] not in ['\n', '', '\t', '\r'] else ''  # to avoid empty marks
                inner_html += tag + text[0:i] + mark + text[j:len(text)]
            else:
                inner_html += tag + text

    return inner_html
    """
    pass
