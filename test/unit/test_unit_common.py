from checkbot.common.domain import *
from checkbot.common.utils import *


def test_element():
    assert not Element()

    element = Element()
    element.__id__ = '1'
    element.mask = '#articolo1'
    element.text = 'Articolo   1'
    element.text_h = 'Articolo 1'
    element.html = '<p>Articolo   1</p>'
    element.styles = ['.p1{font-weight: bold;}']
    assert element

    element = Element('1', '#articolo1', 'Articolo   1', 'Articolo 1', '<p>Articolo   1</p>', ['.p1{font-weight: bold;}'])
    assert element
    assert '1' == element.__id__
    assert '#articolo1' == element.mask
    assert 'Articolo   1' == element.text
    assert 'Articolo 1' == element.text_h
    assert '<p>Articolo   1</p>' == element.html
    assert ['.p1{font-weight: bold;}'] == element.styles


def test_error():
    error = Error()
    error.contract_item = 'P0'
    error.error_type = 'M'
    error.x = [(125, 128)]
    error.y = [(0, 351)]
    assert error

    error = Error('P0', 'M', [(125, 128)], [(0, 351)])
    assert error
    assert 'P0' == error.contract_item
    assert 'M' == error.error_type
    assert (125, 128) == error.x[0]
    assert (0, 351) == error.y[0]


def test_item():
    article = Element('1', 'articolo', 'Articolo   1', 'Articolo 1', '<p>Articolo   1</p>', ['.p1{font-weight: bold;}'])
    paragraph = Element('1.1', 'firstparagraph', '1.1 First Paragraph', '1.1 First Paragraph', '<p><span>1.1</span><span> </span>First Paragraph</p>', ['.p1{font-weight: bold;}'])
    sub_paragraph = Element('1.1.1', 'firstsub-paragraph', '1.1.1 First Sub-paragraph', '1.1.1 First Sub-paragraph', '<p><span>1.1.1</span><span> </span>First Sub-paragraph</p>', ['.p1{font-weight: bold;}'])
    sub_sub_paragraph = Element('1.1.1.1', 'firstsub-sub-paragraph', '1.1.1.1 First Sub-sub-paragraph', '1.1.1.1 First Sub-sub-paragraph', '<p><span>1.1.1.1</span><span> </span>First Sub-sub-paragraph</p>', ['.p1{font-weight: bold;}'])

    item = Item(article, paragraph, sub_paragraph, sub_sub_paragraph)
    assert item

    item.add('Articolo   1', 'Articolo 1', '<p>Articolo   1</p>', ['.p1{font-weight: bold;}'])
    assert 1 == len(item.content)

    assert ['.p1{font-weight: bold;}'] == item.styles

    _element, _ = item.search('Articolo   1')
    assert _element

    assert ['Articolo 1'] == item.to_array()

    assert Constant.SEPARATOR_MASK.join(['articolo', 'firstparagraph', 'firstsub-paragraph', 'firstsub-sub-paragraph']) == Item.format_key(article, paragraph, sub_paragraph, sub_sub_paragraph)


def test_pattern():
    assert '^{}.*?([1-9]{{1}}[0-9]{{0,1}})\s*([a-zA-Z]+)' == Pattern.ARTICLE_1
    assert '^([1-9]{1}[0-9]{0,1})\s*\.*\s*([A-Z]+)' == Pattern.ARTICLE_2
    assert '(^{}.*?([1-9]{{1}}[0-9]{{0,1}})\s*([a-zA-Z]+)|^([1-9]{1}[0-9]{0,1})\s*\.*\s*([A-Z]+))' == Pattern.ARTICLE
    assert '^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)' == Pattern.PARAGRAPH
    assert '^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)' == Pattern.SUB_PARAGRAPH
    assert '^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)' == Pattern.SUB_SUB_PARAGRAPH
    assert '(.*)' == Pattern.TEXT
    assert '([0-9]+)' == Pattern.INT
    assert '([0-9]+[.,][0-9]+)' == Pattern.FLOAT
    assert '(([0-9]+[.,][0-9]+)|([0-9]+))' == Pattern.NUMERIC
    assert '((0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.]((19|20)\d\d|\d\d))' == Pattern.DATE
    assert '((?:{}))' == Pattern.ENUM


def test_constant():
    assert 'A' == Constant.ARTICLE_KEY
    assert 'P' == Constant.PARAGRAPH_KEY
    assert 'S' == Constant.SUB_PARAGRAPH_KEY
    assert 'X' == Constant.SUB_SUB_PARAGRAPH_KEY

    assert '<article>' == Constant.ARTICLE_MASK
    assert '<paragraph>' == Constant.PARAGRAPH_MASK
    assert '<sub_paragraph>' == Constant.SUB_PARAGRAPH_MASK
    assert '<sub_sub_paragraph>' == Constant.SUB_SUB_PARAGRAPH_MASK
    assert '<text>' == Constant.TEXT_MASK
    assert '<int>' == Constant.INT_MASK
    assert '<float>' == Constant.FLOAT_MASK
    assert '<numeric>' == Constant.NUMERIC_MASK
    assert '<date>' == Constant.DATE_MASK
    assert '<enum:(.*)>' == Constant.ENUM_MASK

    assert 'IGNORED_COLS' == Constant.IGNORED_COLS
    assert 'ENUMS' == Constant.ENUMS
    assert 'PARAMS' == Constant.PARAMS
    assert 'TABLES' == Constant.TABLES

    assert 'HEADER' == Constant.HEADER
    assert 'ERROR_MESSAGE' == Constant.ERROR_MESSAGE
    assert 'ESCAPED_CHARACTERS' == Constant.ESCAPED_CHARACTERS
    assert 'NA' == Constant.CONTRACT_ITEM_NON_AVAILABLE
    assert 'M' == Constant.ERROR_MISSING_VALUE
    assert 'C' == Constant.ERROR_WRONG_VALUE


def test_get_pattern():
    assert '.*?(hi\\ ((0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.]((19|20)\d\d|\d\d))\\ there\\.).*?' == get_pattern('Hi <date> there.')
    assert '.*?(hi\\ (^{}.*?([1-9]{{1}}[0-9]{{0,1}})\s*([a-zA-Z]+)|^([1-9]{1}[0-9]{0,1})\s*\.*\s*([A-Z]+))\\ there\\.).*?' == get_pattern('Hi <article> there.')
    assert '.*?(hi\\ ^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)\\ there\\.).*?' == get_pattern('Hi <paragraph> there.')
    assert '.*?(hi\\ ^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)\\ there\\.).*?' == get_pattern('Hi <sub_paragraph> there.')
    assert '.*?(hi\\ ^([1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1}\.[1-9]{1}[0-9]{0,1})\s*([A-Z]+|$)\\ there\\.).*?' == get_pattern('Hi <sub_sub_paragraph> there.')
    assert '.*?(hi\\ (.*)\\ there\\.).*?' == get_pattern('Hi <text> there.')
    assert '.*?(hi\\ ([0-9]+)\\ there\\.).*?' == get_pattern('Hi <int> there.')
    assert '.*?(hi\\ ([0-9]+[.,][0-9]+)\\ there\\.).*?' == get_pattern('Hi <float> there.')
    assert '.*?(hi\\ (([0-9]+[.,][0-9]+)|([0-9]+))\\ there\\.).*?' == get_pattern('Hi <numeric> there.')
    assert '.*?(hi\\ ((?:june|july))\\ there\\.).*?' == get_pattern('Hi <enum:MONTH> there.', {'MONTH': ['June', 'July']})


def test_get_value():
    dictionary = {'AB cde': 'Value 1', 'Aba': 'Value 2', 'Something <numeric>': 'Value 3'}

    assert {} == get_value(dictionary, 'Ab')
    assert 'Value 1' == get_value(dictionary, 'AB cde')
    assert 'Value 2' == get_value(dictionary, 'Aba cde')
    assert 'Value 3' == get_value(dictionary, 'Something 235')


def test_get_html():
    from lxml import etree

    parser = etree.HTMLParser(remove_comments=True)
    element = etree.fromstring('<p>1.1.1   <span>Sub article ABC</span>.</p>', parser)

    assert '<html><body><p>1.1.1   <span>Sub article ABC</span>.</p></body></html>' == get_html(element)


def test_get_text():
    parser = etree.HTMLParser(remove_comments=True)
    element = etree.fromstring('<p>1.1.1   <span>Sub article ABC</span>.</p>', parser)
    text, text_h = get_text(element)

    assert '1.1.1   Sub article ABC.' == text
    assert '1.1.1 Sub article ABC.' == text_h


def test_get_styles():
    from lxml import etree

    parser = etree.HTMLParser(remove_comments=True)
    styles = '.p1{margin-top:0.083333336in;text-align:justify;hyphenate:auto;font-family:Times New Roman;font-size:8pt;}.s1{color:black;}'
    element = etree.fromstring('<p class="p1">1.1.1   <span class="s1">Sub article ABC</span>.</p>', parser)

    assert ['.p1{margin-top:0.083333336in;text-align:justify;hyphenate:auto;font-family:Times New Roman;font-size:8pt;}', '.s1{color:black;}'] == get_styles(element, styles)


def test_format_filename():
    assert 'c/documents/abc.do' == format_filename('c/documents', 'abc.do')
    assert 'c/documents/abc.do' == format_filename('c\\documents\\', 'abc.do')


def test_get_file_name():
    assert 'abc' == get_file_name('abc.do')


def test_get_file_extension():
    assert 'do' == get_file_extension('abc.do')
