def test_get_info():
    """
    text, _ = get_text(element)
    text = text.strip()

    sub_paragraph = re.compile(Pattern.SUB_PARAGRAPH).search(text)
    if sub_paragraph:
        return Constant.SUB_PARAGRAPH_KEY, sub_paragraph.group(1), re.sub(Pattern.SUB_PARAGRAPH, '#', text).replace(' ', '')

    paragraph = re.compile(Pattern.PARAGRAPH).search(text)
    if paragraph:
        return Constant.PARAGRAPH_KEY, paragraph.group(1), re.sub(Pattern.PARAGRAPH, '#', text).replace(' ', '')

    article = re.compile(Pattern.ARTICLE_1.format('|'.join(header))).search(text.lower())
    if article:
        return Constant.ARTICLE_KEY, article.group(1), re.sub(Pattern.ARTICLE_1, '#', text.lower()).replace(' ', '')

    article = re.compile(Pattern.ARTICLE_2).search(text)
    if article:
        # Check style to spot Articles, only for simple pattern e.g 1 Savings
        classes = [element.attrib.get('class'), element.find('span').attrib.get('class')] if element.find('span') is not None else [element.attrib.get('class')]

        for cls in classes:
            if not cls:
                continue
            p = re.compile('.{}{{(.*?)}}'.format(cls)).search(style)
            if p and ('bold' in p.group(1) or 'underline' in p.group(1)):
                return Constant.ARTICLE_KEY, article.group(1), re.sub(Pattern.ARTICLE_2, '#', text).replace(' ', '')

        # return Constant.ARTICLE_KEY, article.group(1), re.sub(Pattern.ARTICLE_2, '#', text).replace(' ', '')

    return None, None, None
    """
    pass


def test_check_current_id():
    """
    if not cur_id or not cur_type:
        return False

    # Fix: When the HTML comes with different enumeration

    if cur_type == Constant.ARTICLE_KEY:
        article_id = int(cur_id)

        prev = prevs.get(Constant.ARTICLE_KEY)
        prev_article_id = int(prev)

        if not (article_id == prev_article_id + 1):
            sys.stdout.write('The article is misnumbered. Got {}, should be {}.\n'.format(article_id, prev_article_id + 1))

        return True

    if cur_type == Constant.PARAGRAPH_KEY:
        article_id = int(cur_id[0:cur_id.find('.')])
        paragraph_id = int(cur_id[cur_id.rfind('.')+1:])

        prev = prevs.get(Constant.PARAGRAPH_KEY)
        prev_article_id = int(prev[0:prev.find('.')])
        prev_paragraph_id = int(prev[prev.rfind('.')+1:])

        if not (article_id == prev_article_id and paragraph_id == prev_paragraph_id + 1):
            sys.stdout.write('The paragraph is misnumbered. Got {}, should be {}.\n'.format('.'.join([str(article_id), str(paragraph_id)]), '.'.join([str(prev_article_id), str(prev_paragraph_id + 1)])))

        return True

    if cur_type == Constant.SUB_PARAGRAPH_KEY:
        article_id = int(cur_id[0:cur_id.find('.')])
        paragraph_id = int(cur_id[cur_id.find('.')+1:cur_id.rfind('.')])
        sub_paragraph_id = int(cur_id[cur_id.rfind('.') + 1:])

        prev = prevs.get(Constant.SUB_PARAGRAPH_KEY)
        prev_article_id = int(prev[0:prev.find('.')])
        prev_paragraph_id = int(prev[prev.find('.')+1:prev.rfind('.')])
        prev_sub_paragraph_id = int(prev[prev.rfind('.') + 1:])

        if not (article_id == prev_article_id and paragraph_id == prev_paragraph_id and sub_paragraph_id == prev_sub_paragraph_id + 1):
            sys.stdout.write('The sub-paragraph is misnumbered. Got {}, should be {}.\n'.format('.'.join([str(article_id), str(paragraph_id), str(sub_paragraph_id)]), '.'.join([str(prev_article_id), str(prev_paragraph_id), str(prev_sub_paragraph_id + 1)])))

        return True

    return False
    """
    pass
