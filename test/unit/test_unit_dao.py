from checkbot.dao.core import *


def test_get_connection(test_client):
    assert get_connection()


def test_select_conf(test_client):
    assert select_conf('IT_it', 'PARAMS')


def test_select_message(test_client):
    assert select_message('IT_it', 'HEADER')


def test_select_header(test_client):
    assert select_header('IT_it')


def test_select_escaped_characters(test_client):
    assert select_escaped_characters()


def test_select_error_messages(test_client):
    assert select_error_messages('IT_it')


def test_select_generic(test_client):
    assert select_generic('IT_it', 'PARAMS')


def test_select_ignored_cols(test_client):
    assert select_ignored_cols('IT_it') or not select_ignored_cols('IT_it')
