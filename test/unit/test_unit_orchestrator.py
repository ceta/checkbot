from checkbot.common.domain import *


def test_build(test_client):
    from checkbot.orchestrator.core import build
    article = Element('1', 'articolo', 'Articolo   1', 'Articolo 1', '<p>Articolo   1</p>', ['.p1{font-weight: bold;}'])
    paragraph = Element('1.1', 'firstparagraph', '1.1 First Paragraph', '1.1 First Paragraph', '<p><span>1.1</span><span> </span>First Paragraph</p>', ['.p1{font-weight: bold;}'])
    sub_paragraph = Element('1.1.1', 'firstsub-paragraph', '1.1.1 First Sub-paragraph', '1.1.1 First Sub-paragraph', '<p><span>1.1.1</span><span> </span>First Sub-paragraph</p>', ['.p1{font-weight: bold;}'])
    sub_sub_paragraph = Element('1.1.1.1', 'firstsub-sub-paragraph', '1.1.1.1 First Sub-sub-paragraph', '1.1.1.1 First Sub-sub-paragraph', '<p><span>1.1.1.1</span><span> </span>First Sub-sub-paragraph</p>', ['.p1{font-weight: bold;}'])
    item = Item(article, paragraph, sub_paragraph, sub_sub_paragraph)
    item.add('Some    text   paragraph', 'Some text paragraph', '<p>Some    <span class="s1">text</span>   paragraph</p>', ['.s1{font-weight: bold;}'])
    element = Element(None, 'articolo||||firstparagraph||||firstsub-paragraph||||firstsub-sub-paragraph', 'Some    text   paragraph', 'Some text paragraph', '<p>Some    <span class="s1">text</span>   paragraph</p>', ['.s1{font-weight: bold;}'])
    error = Error('n.d', 'Valore errato', [], [])

    _article = Element('1', 'articolo', 'Articolo   1', 'Articolo 1', '<p>Articolo   1</p>', ['.p1{font-weight: bold;}'])
    _paragraph = Element('1.1', 'firstparagraph', '1.1 First Paragraph', '1.1 First Paragraph', '<p><span>1.1</span><span> </span>First Paragraph</p>', ['.p1{font-weight: bold;}'])
    _sub_paragraph = Element('1.1.1', 'firstsub-paragraph', '1.1.1 First Sub-paragraph', '1.1.1 First Sub-paragraph', '<p><span>1.1.1</span><span> </span>First Sub-paragraph</p>', ['.p1{font-weight: bold;}'])
    _sub_sub_paragraph = Element('1.1.1.1', 'firstsub-sub-paragraph', '1.1.1.1 First Sub-sub-paragraph', '1.1.1.1 First Sub-sub-paragraph', '<p><span>1.1.1.1</span><span> </span>First Sub-sub-paragraph</p>', ['.p1{font-weight: bold;}'])
    _item = Item(_article, _paragraph, _sub_paragraph, _sub_sub_paragraph)
    _item.add('Some    dummy   paragraph', 'Some dummy paragraph', '<p>Some    <span class="s1">dummy</span>   paragraph</p>', ['.s1{font-weight: bold;}'])
    _element = Element(None, 'articolo||||firstparagraph||||firstsub-paragraph||||firstsub-sub-paragraph', 'Some    dummy   paragraph', 'Some dummy paragraph', '<p>Some    <span class="s1"><mark style="background-color: yellow">dummy</mark></span>   paragraph</p>', ['.s1{font-weight: bold;}'])

    data = {'base': {'item': item, 'element': element, 'index': 0}, 'updated': {'item': _item, 'element': _element, 'index': 0}, 'error': error}
    expected_result = {'data' : [{'id': 1,
                         'article': 'Articolo   1',
                         'paragraph': '1.1 First Paragraph',
                         'sub_paragraph': '1.1.1 First Sub-paragraph',
                         'base_text': '<p>Some    <span class="s1">text</span>   paragraph</p>',
                         'base_text_style': '.s1{font-weight: bold;}',
                         'updated_text': '<p>Some    <span class="s1">dummy</span>   paragraph</p>',
                         'updated_text_style': '.s1{font-weight: bold;}',
                         'contract_item': 'n.d',
                         'error_type': 'Valore errato'}]}

    assert expected_result == build({'data': [data]})
