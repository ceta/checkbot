import pytest
import requests


"""
All request are made to the host and port specified as parameters. This suite intends to act as an integration suite test.
Additionally, a test using a context provided by a python fixture is used, however, in this scenario, the converter service
is not mocked, as in the functional tests.

To change the default host and port use the option --host and --port accordingly:

    - python -m pytest test/init --host 0.0.0.0 --port 9000         OR
    - python -m pytest --host 0.0.0.0 --port 9000

"""


def test_aliveness(host, port):
    req = requests.get('http://{}:{}/aliveness/ping'.format(host, port))

    assert 'OK' == req.text


def test_get_orchestrator(host, port):
    req = requests.get('http://{}:{}/checkbot/IT_it'.format(host, port))

    assert """
                <!DOCTYPE html>
                <title>Upload new File</title>
                <h1>Upload new File</h1>
                <form action="" method="POST" enctype="multipart/form-data">
                    <p><input type="file" name="base_contract"></p>
                    <p><input type="file" name="updated_contract"></p>
                    <input type="submit" value="Upload">
                </form>
                """ == req.text


def test_post_orchestrator(host, port):
    data = {'base_contract': ('base.doc', open('test/data/base.doc', 'rb')), 'updated_contract': ('updated.doc', open('test/data/updated.doc', 'rb'))}
    req = requests.post('http://{}:{}/checkbot/IT_it'.format(host, port), files=data)
    assert 'Valore errato' in req.text


def test_post_orchestrator_with_fixture(test_client):
    data = {'base_contract': ('test/data/base.doc', 'base.doc'), 'updated_contract': ('test/data/updated.doc', 'updated.doc')}
    response = test_client.post('/checkbot/IT_it', content_type='multipart/form-data', data=data)
    assert response.status_code == 200
    assert b'<td>Valore errato</td>' in response.data
