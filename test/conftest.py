import pytest

from flask import Flask

"""
To run the entire test suite:

    - python -m pytest
    
To run a specific scope:

    - python -m pytest test/int
    - python -m pytest test/func
    - python -m pytest test/unit

"""


def pytest_addoption(parser):
    parser.addoption('--host', metavar='host', default='127.0.0.1')
    parser.addoption('--port', metavar='port', default='5000')


@pytest.fixture
def host(request):
    return request.config.getoption('--host')


@pytest.fixture
def port(request):
    return request.config.getoption('--port')


@pytest.fixture(scope='session')
def test_client():
    flask_app = create_flask_app()
    testing_client = flask_app.test_client()

    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client  # this is where the testing happens!

    ctx.pop()


def create_flask_app():
    import checkbot

    checkbot.app = Flask(__name__)
    checkbot.app.config.from_envvar('CHECKBOT_CONFIG')
    checkbot.app.config['DB_FILENAME'] = 'test/data/checkbot.db'
    checkbot.app.config['TESTING'] = True
    checkbot.app.config['DEBUG'] = False
    checkbot.app.config['RENDER'] = True

    import checkbot.orchestrator.core

    return checkbot.app
