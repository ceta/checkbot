from unittest.mock import Mock, patch


"""
Functional test for the flask app. All tests use a python fixture to start a <<test>> flask application.
"""


def test_aliveness(test_client):
    response = test_client.get('/aliveness/ping')
    assert response.status_code == 200
    assert b'OK' in response.data


def test_get_orchestrator(test_client):
    response = test_client.get('/checkbot')
    assert response.status_code == 200
    assert b"""
                <!DOCTYPE html>
                <title>Upload new File</title>
                <h1>Upload new File</h1>
                <form action="" method="POST" enctype="multipart/form-data">
                    <p><input type="file" name="base_contract"></p>
                    <p><input type="file" name="updated_contract"></p>
                    <input type="submit" value="Upload">
                </form>
                """ in response.data


@patch('checkbot.orchestrator.core.convert')
def test_post_orchestrator(mock, test_client):
    mock.return_value = mock_convert_response()

    data = {'base_contract': ('test/data/base.doc', 'base.doc'), 'updated_contract': ('test/data/updated.doc', 'updated.doc')}
    response = test_client.post('/checkbot/IT_it', content_type='multipart/form-data', data=data)
    assert response.status_code == 200

    assert b"""<td colspan="7" style="text-align: left">Nothing to show</td>""" in response.data


def mock_convert_response():
    response = Mock()
    response.status_code = 200
    response.content = b'<html><head><style>.b1{color: red;}</style></head><body><p>Hello world</p></body></html>'
    return response
