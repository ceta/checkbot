"""
Functional test for main processes.
"""


def test_extract(test_client):
    from checkbot.extractor.core import extract
    from checkbot.common.utils import Constant

    base, updated = convert()
    result = extract('IT_it', base, updated)

    # Evaluating structure
    assert result.get('base')
    assert result.get('updated')

    # Evaluating Object creation
    base = result.get('base')
    assert base.get('')
    assert base.get('').article.__id__ == '0'
    assert base.get('premesse,definizioniecostruzioni')
    assert base.get('premesse,definizioniecostruzioni').article.__id__ == '1'
    assert base.get('rilasciodigaranzie')
    assert base.get('rilasciodigaranzie').article.__id__ == '11'
    assert base.get('puntidiconsegnaecapacitàgiornaliera' + Constant.SEPARATOR_MASK + 'puntidiconsegna').paragraph.text == '3.1Punti di Consegna'

    # Evaluating process
    t_punti_di_consegna = '<table class="t1"><tbody><tr><td class="td1" rowspan="1" colspan="1"><p class="p15"><span class="s2">Sito</span></p></td><td class="td2" rowspan="1" colspan="1"><p class="p15"><span class="s2">Ragione</span></p><p class="p15"><span class="s2">sociale</span></p></td><td class="td3" rowspan="1" colspan="1"><p class="p15"><span class="s2">Punto</span></p><p class="p15"><span class="s2">di Consegna su Rete di Trasporto</span></p></td><td class="td4" rowspan="1" colspan="1"><p class="p15"><span class="s2">Indirizzo</span></p></td><td class="td5" rowspan="1" colspan="1"><p class="p15"><span class="s2">Comune</span></p></td><td class="td6" rowspan="1" colspan="1"><p class="p16"><span class="s2">Trasportatore</span></p></td></tr><tr><td class="td1" rowspan="1" colspan="1"><p class="p17"><span>1</span></p></td><td class="td2" rowspan="1" colspan="1"><p class="p17"><span>Test LN SPA</span></p></td><td class="td3" rowspan="1" colspan="1"><p class="p17"><span>34647100</span></p></td><td class="td4" rowspan="1" colspan="1"><p class="p17"><span>via roma</span></p></td><td class="td5" rowspan="1" colspan="1"><p class="p17"><span>Ponte Gardena</span></p></td><td class="td6" rowspan="1" colspan="1"><p class="p17"><span>Snam Rete Gas</span></p></td></tr></tbody></table>'
    assert t_punti_di_consegna == base.get('puntidiconsegnaecapacitàgiornaliera' + Constant.SEPARATOR_MASK + 'puntidiconsegna').content[2].html
    p_fix = '<p class="p20"><span class="s8">​ </span><span>P</span><span class="s10">FIX</span><span> = 3  centesimi di euro/m³;</span></p>'
    assert p_fix == base.get('prezzo' + Constant.SEPARATOR_MASK + 'prezzoperilgas').content[4].html


def test_compare(test_client):
    from checkbot.extractor.core import extract
    from checkbot.comparator.core import compare

    base, updated = convert()
    result = compare('IT_it', extract('IT_it', base, updated))

    assert result.get('data')

    # Evaluating errors
    data = result.get('data')
    assert 6 == len(data)
    b_data_inizio_somministrazione = '<span>La somministrazione di gas ai sensi del presente Contratto decorre dal 01/07/201<mark style="background-color: yellow">8</mark> </span>'
    error_0_b = data[0].get('base').get('element')
    assert b_data_inizio_somministrazione in error_0_b.html
    u_data_inizio_somministrazione = '<span>La somministrazione di gas ai sensi del presente Contratto decorre dal 01/07/201<mark style="background-color: yellow">9</mark> </span>'
    error_0_u = data[0].get('updated').get('element')
    assert u_data_inizio_somministrazione in error_0_u.html

    assert 'Supply Period - End Date' == data[0].get('error').contract_item
    assert 'Valore errato' == data[0].get('error').error_type
    assert 'Supply Period - Start Date' == data[1].get('error').contract_item
    assert 'Valore errato' == data[1].get('error').error_type
    assert 'n.d' == data[2].get('error').contract_item
    assert 'Valore errato' == data[2].get('error').error_type
    assert 'PFix' == data[3].get('error').contract_item
    assert 'Valore errato' == data[3].get('error').error_type
    assert 'P0' == data[4].get('error').contract_item
    assert 'Valore errato' == data[4].get('error').error_type
    assert 'Cost for Overtake Table' == data[5].get('error').contract_item
    assert 'Valore errato' == data[5].get('error').error_type


def test_render(test_client):
    from checkbot.extractor.core import extract
    from checkbot.comparator.core import compare
    from checkbot.renderer.core import render
    from checkbot.orchestrator.core import build

    base, updated = convert()
    result = render(build(compare('IT_it', extract('IT_it', base, updated))))

    assert 'html' in result
    assert 'head' in result
    assert 'body' in result
    assert 'table' in result
    assert 'Supply Period - End Date' in result
    assert 'Supply Period - Start Date' in result
    assert '<td>n.d</td>' in result
    assert '<td>P0</td>' in result
    assert '<td>PFix</td>' in result
    assert '<td>Cost for Overtake Table</td>' in result
    assert '<td>Valore errato</td>' in result
    assert '<td>2Periodo Contrattuale</td>' in result
    assert '<td>3Punti di Consegna e Capacità giornaliera</td>' in result
    assert '<td>4Prezzo</td>' in result
    assert '<td>6Superamento della Capacità Giornaliera</td>' in result


def convert():
    return open('test/data/base.html', encoding='utf-8').read(), open('test/data/updated.html', encoding='utf-8').read()

